#!/bin/bash
make
P=./latc_x86
gok=0
gall=0
tmp=______temporary_____
for i in `find tests/good/core -name "*.lat"` \
         `find tests/good/basic -name "*.lat"` \
         `find tests/opt/ -name "*.lat"` \
         `find tests/good/arrays/ -name "*.lat"` \
         `find tests/good/arrays1 -name "*.lat"` \
         `find tests/good/objects/ -name "*.lat"`
do
    gall=$(($gall + 1))
    ex=${i/.lat/}
    in=${i/lat/input}
    touch $in
    out=${i/lat/output}
    touch $out
    if $P $i &>/dev/null && ($ex < $in > $tmp || echo "") && diff $tmp $out
    then
        gok=$(($gok + 1))
        echo $i " OK"
    else
        echo $i " FAIL"
    fi
    rm -f ${i/lat/s}
    rm -f $ex
    rm -f $tmp
done

bok=0
ball=0
for i in `find tests/bad/core/ -name "*.lat"` \
         `find tests/bad/structs -name "*.lat"`
do
    ball=$(($ball + 1))
    ex=${i/.lat/}
    if ! $P $i &>/dev/null
    then
        bok=$(($bok + 1))
        echo $i " OK"
    else
        echo $i " FAIL"
    fi
    rm -f $ex
done

sok=0
sall=1
err=tests/others/error-return.lat
ex=${err/.lat/}
out=${err/lat/output}
if $P $err &>/dev/null && ! $ex &> $tmp && diff $tmp $out
then
    sok=$(($sok + 1))
    echo $err " OK"
else
    echo $err " FAIL"
fi
rm -f $tmp
rm -f $ex
rm -f ${i/lat/s}

echo "Good tests: $gok/$gall"
echo "Bad tests: $bok/$ball"
echo "Special tests: $sok/$sall"
