section .data
fmti db "%d", 0xa, 0
fmts db "%s", 0xa, 0
sfmti db "%d", 0xa, 0
err db "runtime error", 0xa, 0

section .text
global printInt, printString, error, readInt, readString, __concat_strings__
global __compare_strings__, __at__exit__

extern printf, scanf, stdout, getline, exit, strlen, strcat, malloc, strcpy
extern stdin, strcmp, fflush

printInt:
    push ebp
    mov ebp, esp
    push dword [ebp + 8]
    push dword fmti
    call printf
    pop ecx
    pop ecx
    mov esp, ebp
    pop ebp
    ret

printString:
    push ebp
    mov ebp, esp
    push dword [ebp + 8]
    push dword fmts
    call printf
    pop ecx
    pop ecx
    mov esp, ebp
    pop ebp
    ret

readInt:
    push ebp
    mov ebp, esp
    sub esp, 4
    lea ecx, [ebp - 4]
    push dword ecx
    push dword sfmti
    call scanf
    pop ecx
    pop ecx
    mov eax, [ebp - 4]
    mov esp, ebp
    pop ebp
    ret

readString:
    push ebp
    mov ebp, esp
    sub esp, 8
    mov dword [ebp - 4], 0
    mov dword [ebp - 8], 0
    push dword [stdin]
    lea ecx, [ebp - 8]
    push ecx
    lea ecx, [ebp - 4]
    push ecx
    call getline
    pop ecx
    pop ecx
    pop ecx
    cmp eax, -1
    je error

    mov ecx, [ebp - 4]
    mov byte [ecx + eax - 1], 0
    mov eax, ecx
    mov esp, ebp
    pop ebp
    ret

error:
    push err
    call printf
    pop ecx
    push (-1)
    call exit

__concat_strings__:
    push ebp
    mov ebp, esp
    sub esp, 4
    push ebx

    push dword [ebp + 8]
    call strlen
    pop ecx
    mov ebx, eax

    push dword [ebp + 12]
    call strlen
    pop edx

    add ebx, eax
    inc ebx

    push ebx
    call malloc
    pop ebx
    mov [ebp - 4], eax

    push dword [ebp + 8]
    push eax
    call strcpy
    pop ecx
    pop ecx

    push dword [ebp + 12]
    push dword [ebp - 4]
    call strcat
    pop ecx
    pop ecx

    pop ebx
    mov esp, ebp
    pop ebp
    ret

__compare_strings__:
    push ebp
    mov ebp, esp

    push dword [ebp + 12]
    push dword [ebp + 8]
    call strcmp
    pop ecx
    pop ecx

    mov ecx, 0
    cmp eax, 0
    sete cl
    mov eax, ecx

    mov esp, ebp
    pop ebp
    ret

__at__exit__:
    push dword [stdout]
    call fflush
    pop ecx
    push dword [ebp + 8]
    call exit
