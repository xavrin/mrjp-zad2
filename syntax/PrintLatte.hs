{-# OPTIONS_GHC -fno-warn-incomplete-patterns #-}
module PrintLatte where

-- pretty-printer generated by the BNF converter

import AbsLatte
import Data.Char


-- the top-level printing method
printTree :: Print a => a -> String
printTree = render . prt 0

type Doc = [ShowS] -> [ShowS]

doc :: ShowS -> Doc
doc = (:)

render :: Doc -> String
render d = rend 0 (map ($ "") $ d []) "" where
  rend i ss = case ss of
    "["      :ts -> showChar '[' . rend i ts
    "("      :ts -> showChar '(' . rend i ts
    "{"      :ts -> showChar '{' . new (i+1) . rend (i+1) ts
    "}" : ";":ts -> new (i-1) . space "}" . showChar ';' . new (i-1) . rend (i-1) ts
    "}"      :ts -> new (i-1) . showChar '}' . new (i-1) . rend (i-1) ts
    ";"      :ts -> showChar ';' . new i . rend i ts
    t  : "," :ts -> showString t . space "," . rend i ts
    t  : ")" :ts -> showString t . showChar ')' . rend i ts
    t  : "]" :ts -> showString t . showChar ']' . rend i ts
    t        :ts -> space t . rend i ts
    _            -> id
  new i   = showChar '\n' . replicateS (2*i) (showChar ' ') . dropWhile isSpace
  space t = showString t . (\s -> if null s then "" else (' ':s))

parenth :: Doc -> Doc
parenth ss = doc (showChar '(') . ss . doc (showChar ')')

concatS :: [ShowS] -> ShowS
concatS = foldr (.) id

concatD :: [Doc] -> Doc
concatD = foldr (.) id

replicateS :: Int -> ShowS -> ShowS
replicateS n f = concatS (replicate n f)

-- the printer class does the job
class Print a where
  prt :: Int -> a -> Doc
  prtList :: [a] -> Doc
  prtList = concatD . map (prt 0)

instance Print a => Print [a] where
  prt _ = prtList

instance Print Char where
  prt _ s = doc (showChar '\'' . mkEsc '\'' s . showChar '\'')
  prtList s = doc (showChar '"' . concatS (map (mkEsc '"') s) . showChar '"')

mkEsc :: Char -> Char -> ShowS
mkEsc q s = case s of
  _ | s == q -> showChar '\\' . showChar s
  '\\'-> showString "\\\\"
  '\n' -> showString "\\n"
  '\t' -> showString "\\t"
  _ -> showChar s

prPrec :: Int -> Int -> Doc -> Doc
prPrec i j = if j<i then parenth else id


instance Print Integer where
  prt _ x = doc (shows x)


instance Print Double where
  prt _ x = doc (shows x)


instance Print Ident where
  prt _ (Ident i) = doc (showString ( i))
  prtList es = case es of
   [x] -> (concatD [prt 0 x])
   x:xs -> (concatD [prt 0 x , doc (showString ",") , prt 0 xs])



instance Print Prog where
  prt i e = case e of
   PProg topdefs -> prPrec i 0 (concatD [prt 0 topdefs])


instance Print TopDef where
  prt i e = case e of
   TopFnDef fndef -> prPrec i 0 (concatD [prt 0 fndef])
   TopClDefB id mems -> prPrec i 0 (concatD [doc (showString "class") , prt 0 id , doc (showString "{") , prt 0 mems , doc (showString "}")])
   TopClDefEx id0 id mems -> prPrec i 0 (concatD [doc (showString "class") , prt 0 id0 , doc (showString "extends") , prt 0 id , doc (showString "{") , prt 0 mems , doc (showString "}")])

  prtList es = case es of
   [x] -> (concatD [prt 0 x])
   x:xs -> (concatD [prt 0 x , prt 0 xs])

instance Print Mem where
  prt i e = case e of
   MVar type' ids -> prPrec i 0 (concatD [prt 0 type' , prt 0 ids , doc (showString ";")])
   MFun fndef -> prPrec i 0 (concatD [prt 0 fndef])

  prtList es = case es of
   [x] -> (concatD [prt 0 x])
   x:xs -> (concatD [prt 0 x , prt 0 xs])

instance Print FnDef where
  prt i e = case e of
   FFnDef type' id args block -> prPrec i 0 (concatD [prt 0 type' , prt 0 id , doc (showString "(") , prt 0 args , doc (showString ")") , prt 0 block])


instance Print Arg where
  prt i e = case e of
   AArg type' id -> prPrec i 0 (concatD [prt 0 type' , prt 0 id])

  prtList es = case es of
   [] -> (concatD [])
   [x] -> (concatD [prt 0 x])
   x:xs -> (concatD [prt 0 x , doc (showString ",") , prt 0 xs])

instance Print Block where
  prt i e = case e of
   BBlock stmts -> prPrec i 0 (concatD [doc (showString "{") , prt 0 stmts , doc (showString "}")])


instance Print Stmt where
  prt i e = case e of
   SEmpty  -> prPrec i 0 (concatD [doc (showString ";")])
   SBlock block -> prPrec i 0 (concatD [prt 0 block])
   SDecl type' items -> prPrec i 0 (concatD [prt 0 type' , prt 0 items , doc (showString ";")])
   SAss expr0 expr -> prPrec i 0 (concatD [prt 0 expr0 , doc (showString "=") , prt 0 expr , doc (showString ";")])
   SIncr expr -> prPrec i 0 (concatD [prt 0 expr , doc (showString "++") , doc (showString ";")])
   SDecr expr -> prPrec i 0 (concatD [prt 0 expr , doc (showString "--") , doc (showString ";")])
   SRet expr -> prPrec i 0 (concatD [doc (showString "return") , prt 0 expr , doc (showString ";")])
   SVRet  -> prPrec i 0 (concatD [doc (showString "return") , doc (showString ";")])
   SCond expr stmt -> prPrec i 0 (concatD [doc (showString "if") , doc (showString "(") , prt 0 expr , doc (showString ")") , prt 0 stmt])
   SCondElse expr stmt0 stmt -> prPrec i 0 (concatD [doc (showString "if") , doc (showString "(") , prt 0 expr , doc (showString ")") , prt 0 stmt0 , doc (showString "else") , prt 0 stmt])
   SWhile expr stmt -> prPrec i 0 (concatD [doc (showString "while") , doc (showString "(") , prt 0 expr , doc (showString ")") , prt 0 stmt])
   SForeach type' id0 id stmt -> prPrec i 0 (concatD [doc (showString "for") , doc (showString "(") , prt 0 type' , prt 0 id0 , doc (showString ":") , prt 0 id , doc (showString ")") , prt 0 stmt])
   SExp expr -> prPrec i 0 (concatD [prt 0 expr , doc (showString ";")])

  prtList es = case es of
   [] -> (concatD [])
   x:xs -> (concatD [prt 0 x , prt 0 xs])

instance Print Item where
  prt i e = case e of
   SNoInit id -> prPrec i 0 (concatD [prt 0 id])
   SInit id expr -> prPrec i 0 (concatD [prt 0 id , doc (showString "=") , prt 0 expr])

  prtList es = case es of
   [x] -> (concatD [prt 0 x])
   x:xs -> (concatD [prt 0 x , doc (showString ",") , prt 0 xs])

instance Print Type where
  prt i e = case e of
   TBasic btype -> prPrec i 0 (concatD [prt 0 btype])
   TArray atype -> prPrec i 0 (concatD [prt 0 atype])
   TClass ctype -> prPrec i 0 (concatD [prt 0 ctype])
   TFun type' types -> prPrec i 0 (concatD [prt 0 type' , doc (showString "(") , prt 0 types , doc (showString ")")])

  prtList es = case es of
   [] -> (concatD [])
   [x] -> (concatD [prt 0 x])
   x:xs -> (concatD [prt 0 x , doc (showString ",") , prt 0 xs])

instance Print BType where
  prt i e = case e of
   BTInt  -> prPrec i 0 (concatD [doc (showString "int")])
   BTStr  -> prPrec i 0 (concatD [doc (showString "string")])
   BTBool  -> prPrec i 0 (concatD [doc (showString "boolean")])
   BTVoid  -> prPrec i 0 (concatD [doc (showString "void")])


instance Print AType where
  prt i e = case e of
   ATArray type' -> prPrec i 0 (concatD [prt 0 type' , doc (showString "[]")])


instance Print CType where
  prt i e = case e of
   CTClass id -> prPrec i 0 (concatD [prt 0 id])


instance Print Expr where
  prt i e = case e of
   EVar id -> prPrec i 9 (concatD [prt 0 id])
   ELitInt n -> prPrec i 9 (concatD [prt 0 n])
   ELitTrue  -> prPrec i 9 (concatD [doc (showString "true")])
   ELitFalse  -> prPrec i 9 (concatD [doc (showString "false")])
   ENull  -> prPrec i 9 (concatD [doc (showString "null")])
   ESelf  -> prPrec i 9 (concatD [doc (showString "self")])
   EApp id exprs -> prPrec i 9 (concatD [prt 0 id , doc (showString "(") , prt 0 exprs , doc (showString ")")])
   EString str -> prPrec i 9 (concatD [prt 0 str])
   EArray expr0 expr -> prPrec i 8 (concatD [prt 9 expr0 , doc (showString "[") , prt 0 expr , doc (showString "]")])
   EACast atype expr -> prPrec i 8 (concatD [doc (showString "(") , prt 0 atype , doc (showString ")") , prt 9 expr])
   EExprCast expr0 expr -> prPrec i 8 (concatD [doc (showString "(") , prt 0 expr0 , doc (showString ")") , prt 9 expr])
   EClInit type' -> prPrec i 7 (concatD [doc (showString "new") , prt 0 type'])
   EArrInit type' expr -> prPrec i 7 (concatD [doc (showString "new") , prt 0 type' , doc (showString "[") , prt 0 expr , doc (showString "]")])
   EMemVar expr id -> prPrec i 6 (concatD [prt 6 expr , doc (showString ".") , prt 0 id])
   EMemFun expr id exprs -> prPrec i 6 (concatD [prt 6 expr , doc (showString ".") , prt 0 id , doc (showString "(") , prt 0 exprs , doc (showString ")")])
   ENeg expr -> prPrec i 5 (concatD [doc (showString "-") , prt 6 expr])
   ENot expr -> prPrec i 5 (concatD [doc (showString "!") , prt 6 expr])
   EMul expr0 mulop expr -> prPrec i 4 (concatD [prt 4 expr0 , prt 0 mulop , prt 5 expr])
   EAdd expr0 addop expr -> prPrec i 3 (concatD [prt 3 expr0 , prt 0 addop , prt 4 expr])
   EOrd expr0 ordop expr -> prPrec i 2 (concatD [prt 2 expr0 , prt 0 ordop , prt 3 expr])
   EEq expr0 eqop expr -> prPrec i 2 (concatD [prt 2 expr0 , prt 0 eqop , prt 3 expr])
   EAnd expr0 expr -> prPrec i 1 (concatD [prt 2 expr0 , doc (showString "&&") , prt 1 expr])
   EOr expr0 expr -> prPrec i 0 (concatD [prt 1 expr0 , doc (showString "||") , prt 0 expr])

  prtList es = case es of
   [] -> (concatD [])
   [x] -> (concatD [prt 0 x])
   x:xs -> (concatD [prt 0 x , doc (showString ",") , prt 0 xs])

instance Print AddOp where
  prt i e = case e of
   OPlus  -> prPrec i 0 (concatD [doc (showString "+")])
   OMinus  -> prPrec i 0 (concatD [doc (showString "-")])


instance Print MulOp where
  prt i e = case e of
   OTimes  -> prPrec i 0 (concatD [doc (showString "*")])
   ODiv  -> prPrec i 0 (concatD [doc (showString "/")])
   OMod  -> prPrec i 0 (concatD [doc (showString "%")])


instance Print OrdOp where
  prt i e = case e of
   OLTH  -> prPrec i 0 (concatD [doc (showString "<")])
   OLE  -> prPrec i 0 (concatD [doc (showString "<=")])
   OGTH  -> prPrec i 0 (concatD [doc (showString ">")])
   OGE  -> prPrec i 0 (concatD [doc (showString ">=")])


instance Print EqOp where
  prt i e = case e of
   OEQU  -> prPrec i 0 (concatD [doc (showString "==")])
   ONE  -> prPrec i 0 (concatD [doc (showString "!=")])



