module SemCheck where
import AbsLatte
import PrintLatte(printTree)
import Misc(tInt, tString, tBool, tVoid, predefinedFuns, isValidInt,
            unsafeLookup)

import Control.Monad.Trans.Except(ExceptT, runExceptT)
import Control.Monad.Identity(Identity, runIdentity)
import Control.Monad.Trans.Reader(ReaderT, runReaderT, ask, local, asks)
import Control.Monad.Error.Class(throwError)
import Control.Monad.State(get, gets, modify, StateT, runStateT)
import Control.Monad(forM_, foldM, when, unless, void)
import Text.Format(format)
import Data.List(intercalate)
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set

{- the second argument is for the level of nesting -}
type EnvVal = (Type, Int)

type Vars = Map.Map Ident EnvVal

type Class = Map.Map Ident Type

data Env = Env {
    rVars :: Vars, -- identifiers
    rLev :: Int, -- the level of nesting of the current scope
    rStructs :: Map.Map Ident Class,
    rInhGraph :: Map.Map Ident (Maybe Ident),
    rCurCls :: Maybe Ident
} deriving Show

data State = State {
    rReturned :: Bool, -- whether there was a 100%-sure return in the current
                      -- function
    rCurStmt :: Stmt, -- current statement, needed for error printing
    rRetType :: Type -- return type for the current function
}

type SemEval a = ReaderT Env (ExceptT SemError (StateT State Identity)) a

runSemCheck :: SemEval a -> Either SemError a
runSemCheck m = let
    defState = State {rReturned = False,
                      rCurStmt = SBlock $ BBlock [],
                      rRetType = TBasic BTVoid}
    predFuns = map (\(a, b) -> (a, (b, 0))) predefinedFuns
    defEnv = Env {rVars = Map.fromList predFuns, rLev = 0,
                  rStructs = Map.empty,
                  rInhGraph = Map.empty,
                  rCurCls = Nothing}
  in fst $ runIdentity (runStateT (runExceptT (runReaderT m defEnv)) defState)

semCheck :: Prog -> Either SemError ()
semCheck = runSemCheck . checkProg

checkProg :: Prog -> SemEval ()
checkProg (PProg topDefs) = do
    let mainIdent = Ident "main"
    env <- ask
    env' <- foldM checkClassTypes env topDefs
    env'' <- foldM checkTopDefSig env' topDefs
    env''' <- resolveInh (Map.keys (rStructs env'')) env''
    unless (Map.member mainIdent (rVars env''')) (throwSemError NoMain)
    let typ = fst $ unsafeLookup mainIdent (rVars env''')
    let mainType = TFun tInt []
    unless (typ == mainType) (throwSemError $ BadMain typ)
    forM_ topDefs (local (const env''') . checkTopDef)

{- It's needed to eliminate using non-existing classes in class definitions. -}
checkClassTypes :: Env -> TopDef -> SemEval Env
checkClassTypes env (TopClDefB name _) = do
    when (Map.member name (rStructs env)) (throwSemError $ NameNotUnique name)
    return $ env {rStructs = Map.insert name Map.empty (rStructs env)}
checkClassTypes env (TopClDefEx name _ members) =
    checkClassTypes env (TopClDefB name members)
checkClassTypes env _ = return env

checkTopDefSig :: Env -> TopDef -> SemEval Env
checkTopDefSig env (TopClDefB clsName members) = let
    checkMember env' (MVar typ idents) =
        foldM (checkMem clsName typ) env' idents
    checkMember env' (MFun (FFnDef rTyp fName args _)) = let
        argTypes = foldr (\(AArg t _) l -> t : l) [] args
      in checkMem clsName (TFun rTyp argTypes) env' fName
  in do
    env' <- foldM checkMember env members
    return $ env' {rInhGraph = Map.insert clsName Nothing (rInhGraph env')}
checkTopDefSig env (TopClDefEx cls baseCls members) = do
    unless (Map.member baseCls (rStructs env))
           (throwSemError $ NoSuchClass baseCls)
    env' <- checkTopDefSig env (TopClDefB cls members)
    return $ env' {rInhGraph = Map.insert cls (Just baseCls) (rInhGraph env')}
checkTopDefSig env (TopFnDef (FFnDef typ ident args _)) = do
    let argTypes = foldr (\(AArg t _) l -> t : l) [] args
    when (Map.member ident (rVars env)) (throwSemError $ NameNotUnique ident)
    return $ env {rVars = Map.insert ident (TFun typ argTypes, 0) (rVars env)}

checkMem :: Ident -> Type -> Env -> Ident -> SemEval Env
checkMem cls typ env name = do
    {- Otherwise there could occur a conflict with global variables. -}
    void $ checkVar (env {rLev = rLev env + 1}) typ name
    let Just struct = Map.lookup cls (rStructs env)
    when (Map.member name struct) (throwSemError $ NameNotUnique name)
    let struct' = Map.insert name typ struct
    return $ env {rStructs = Map.insert cls struct' (rStructs env)}

resolveInh :: [Ident] -> Env -> SemEval Env
resolveInh classes env = do
    res <- foldM (\(vis, env') cls ->
                  checkInh (Set.singleton cls) cls (Set.insert cls vis, env'))
                 (Set.empty, env) classes
    return (snd res)

checkInh :: Set.Set Ident -> Ident -> (Set.Set Ident, Env) ->
                                      SemEval (Set.Set Ident, Env)
checkInh onStack curCls (visited, env) =
    case unsafeLookup curCls (rInhGraph env) of
        Nothing -> return (visited, env)
        Just baseCls -> do
            when (Set.member baseCls onStack)
                 (throwSemError $ InheritanceCycle curCls)
            (vis', env') <-
                if Set.member baseCls visited then
                    return (visited, env)
                else let
                    visited' = Set.insert baseCls visited
                    onStack' = Set.insert baseCls onStack
                  in checkInh onStack' baseCls (visited', env)
            let base = unsafeLookup baseCls (rStructs env')
            let cur = unsafeLookup curCls (rStructs env')
            cls' <- mergeClasses curCls base cur
            return (vis',
                    env' {rStructs = Map.insert curCls cls' (rStructs env')})

isSubclass :: Ident -> Ident -> SemEval Bool
isSubclass curCls baseCls =
    if curCls == baseCls then
        return True
    else do
        gr <- asks rInhGraph
        case unsafeLookup curCls gr of
            Nothing -> return False
            Just cl -> isSubclass cl baseCls

mergeClasses :: Ident -> Class -> Class -> SemEval Class
mergeClasses clsName baseCls cls =
    foldM (\cl (mem, typ) ->
        case Map.lookup mem cl of
            Nothing -> return $ Map.insert mem typ cl
            Just _ -> throwSemError $ BadInheritance clsName mem)
          baseCls (Map.toList cls)

checkTopDef :: TopDef -> SemEval ()
checkTopDef (TopClDefB cls members) = let
    checkM (MVar _ _) = return ()
    checkM (MFun fndef) = do
        structs <- asks rStructs
        lev <- asks rLev
        env_ <- ask
        let mems = unsafeLookup cls structs
        let types = Map.elems mems
        let tuples = zip types (replicate (length types) (lev + 1))
        let vars' = Map.fromList (zip (Map.keys mems) tuples)
        let vars'' = Map.union vars' (rVars env_)
        local (\env -> env {rCurCls = Just cls,
                            rVars = vars'',
                            rLev = lev + 2}) (checkTopDef (TopFnDef fndef))
  in forM_ members checkM
checkTopDef (TopClDefEx cls _ members) = checkTopDef (TopClDefB cls members)
checkTopDef (TopFnDef (FFnDef typ ident args body)) = do
    env <- ask
    env' <- foldM checkArg (env {rLev = rLev env + 1}) args
    modify (\st -> st {rRetType = typ, rReturned = False})
    local (const env') (checkBlock body)
    b <- gets rReturned
    unless (b || typ == tVoid) (throwSemError $ NoReturn ident)

checkBlock :: Block -> SemEval ()
checkBlock (BBlock stmts) =
    local (\env -> env {rLev = rLev env + 1}) (checkStmts stmts)

checkStmts :: [Stmt] -> SemEval ()
{- In order not to use incorrect statement during showing function errors. -}
checkStmts [] =  modify (\st -> st {rCurStmt = SBlock $ BBlock []})
checkStmts (stmt@(SDecl typ items) : stmts) = do
    modify (\st -> st {rCurStmt = stmt})
    env <- ask
    env' <- foldM (checkItem typ) env items
    local (const env') (checkStmts stmts)
checkStmts [stmt] = do
    modify (\st -> st {rCurStmt = stmt})
    checkStmt stmt
{- here we only check that if there is a return, it is the last instruction -}
checkStmts (stmt : stmts) = do
    modify (\st -> st {rCurStmt = stmt})
    checkStmt stmt
    checkStmts stmts

checkStmt :: Stmt -> SemEval ()
checkStmt SEmpty = return ()
checkStmt (SBlock block) = checkBlock block
checkStmt SDecl {} = error "Internal error: checkStmt"
checkStmt (SAss expr1 expr2) = do
    checkLvalue expr1
    typ1 <- checkExpr expr1
    checkExprTypeValid expr2 typ1
checkStmt (SIncr expr) = do
    checkExprTypeValid expr tInt
    checkLvalue expr
checkStmt (SDecr expr) = checkStmt (SIncr expr)
checkStmt (SRet expr) = do
    st <- get
    typ <- checkExpr expr
    {- We need the line below because null is of type void. -}
    when (rRetType st == tVoid) (throwSemError $ BadType tVoid typ)
    checkExprTypeValid expr (rRetType st)
    modify (\st' -> st' {rReturned = True})
checkStmt SVRet = do
    st <- get
    unless (tVoid == rRetType st) (throwSemError $ BadType tVoid (rRetType st))
    modify (\st' -> st' {rReturned = True})
checkStmt (SCondElse expr stmt1 stmt2) = do
    let updateRet bVal state = state {rReturned = bVal || rReturned state}
    checkExprTypeValid expr tBool
    b1 <- localRetCheck (checkStmt stmt1)
    b2 <- localRetCheck (checkStmt stmt2)
    m <- checkConstBoolExpr expr
    case m of
        Just True -> modify (updateRet b1)
        Just False -> modify (updateRet b2)
        Nothing -> modify (updateRet (b1 && b2))
checkStmt (SCond expr stmt) = checkStmt (SCondElse expr stmt SEmpty)
checkStmt (SWhile expr stmt) = do
    checkExprTypeValid expr tBool
    {- I require the returns to always appear after while. -}
    void $ localRetCheck (checkStmt stmt)
checkStmt (SForeach typ ident1 ident2 stmt) = do
    checkExprTypeValid (EVar ident2) (TArray $ ATArray typ)
    env <- ask
    let env' = env {rVars = Map.insert ident1 (typ, rLev env + 1) (rVars env),
                    rLev = rLev env + 1}
    {- I require the returns to always appear after foreach. -}
    void $ localRetCheck (local (const env') (checkStmt stmt))
checkStmt (SExp expr) = void $ checkExpr expr

{- It returns, whether the monad m returns or not. -}
localRetCheck :: SemEval () -> SemEval Bool
localRetCheck m = do
    b0 <- gets rReturned
    modify (\st -> st {rReturned = False})
    m
    b1 <- gets rReturned
    modify (\st -> st {rReturned = b0})
    return b1

checkExpr :: Expr -> SemEval Type
checkExpr (EVar ident) = do
    env <- ask
    case Map.lookup ident (rVars env) of
        Nothing -> throwSemError $ NoVariable ident
        Just (typ, _) -> return typ
checkExpr (ELitInt int) = do
    unless (isValidInt int) (throwSemError $ IntLitOutOfRange int)
    return tInt
checkExpr ELitTrue = return tBool
checkExpr ELitFalse = return tBool
checkExpr ENull = return tVoid
checkExpr (EApp ident exprs) = do
    env <- ask
    case Map.lookup ident (rVars env) of
        Nothing -> throwSemError $ NoVariable ident
        Just typ -> checkFunCall ident (fst typ) exprs
checkExpr (EString _) = return tString
checkExpr (EArray expr1 expr2) = do
    checkExprTypeValid expr2 tInt
    typ <- checkExpr expr1
    case typ of
        TArray (ATArray typ') -> return typ'
        _ -> throwSemError $ NotArray typ
checkExpr (EACast atyp expr) = do
    checkExprTypeValid expr tVoid
    return $ TArray atyp
checkExpr expr@(EExprCast expr1 expr2) = do
    checkExprTypeValid expr2 tVoid
    case expr1 of
        EVar ident -> do
            env <- ask
            case Map.lookup ident (rStructs env) of
                Just _ -> return (identToType ident)
                _ -> throwSemError $ NoSuchClass ident
        _ -> throwSemError $ BadCast expr
checkExpr (EClInit typ) = return typ
checkExpr (EArrInit typ expr) = do
    checkExprTypeValid expr tInt
    return $ TArray $ ATArray typ
checkExpr (EMemVar expr ident@(Ident name)) = do
    typ <- checkExpr expr
    case typ of
        TArray (ATArray _) -> do
            unless (name == "length") (throwSemError $ NoMember typ ident)
            return tInt
        TClass (CTClass cls) -> do
            structs <- asks rStructs
            let vars = unsafeLookup cls structs
            unless (Map.member ident vars) (throwSemError $ NoMember typ ident)
            return $ unsafeLookup ident vars
        _ -> throwSemError $ NotAClass typ
checkExpr (EMemFun expr name exprs) = do
    typ <- checkExpr expr
    case typ of
        TClass (CTClass cls) -> do
            structs <- asks rStructs
            unless (Map.member cls structs) (throwSemError $ NoSuchClass cls)
            let cl = unsafeLookup cls structs
            unless (Map.member name cl)
                   (throwSemError $ NoMember (identToType cls) name)
            let fTyp = unsafeLookup name cl
            checkFunCall name fTyp exprs
        _ -> throwSemError $ NotAClass typ
checkExpr (ENeg expr) = do
    checkExprTypeValid expr tInt
    return tInt
checkExpr (ENot expr) = do
    checkExprTypeValid expr tBool
    return tBool
checkExpr (EMul expr1 _ expr2) = do
    checkExprTypeValid expr1 tInt
    checkExprTypeValid expr2 tInt
    return tInt
checkExpr (EAdd expr1 OPlus expr2) = do
    typ1 <- checkExpr expr1
    typ2 <- checkExpr expr2
    unless (typ1 == typ2) (throwSemError $ BadPlusArgs typ1 typ2)
    unless (typ1 == tInt || typ1 == tString)
           (throwSemError $ BadPlusArgs typ1 typ2)
    return typ1
checkExpr (EAdd expr1 OMinus expr2) = checkExpr (EMul expr1 OTimes expr2)
checkExpr (EOrd expr1 _ expr2) = do
    checkExprTypeValid expr1 tInt
    checkExprTypeValid expr2 tInt
    return tBool
checkExpr (EEq expr1 _ expr2) = do
    typ1 <- checkExpr expr1
    typ2 <- checkExpr expr2
    unless (typ1 == typ2) (throwSemError $ EqOpError typ1 typ2)
    return tBool
checkExpr (EAnd expr1 expr2) = do
    checkExprTypeValid expr1 tBool
    checkExprTypeValid expr2 tBool
    return tBool
checkExpr (EOr expr1 expr2) = checkExpr (EAnd expr1 expr2)
checkExpr ESelf = do
    env <- ask
    case rCurCls env of
        Nothing -> throwSemError UsingThisOutsideClass
        Just clsName -> return $ identToType clsName

checkFunCall :: Ident -> Type -> [Expr] -> SemEval Type
checkFunCall _ (TFun retTyp typs) exprs = do
    let (tl, el) = (length typs, length exprs)
    unless (tl == el) (throwSemError $ WrongArgsNum tl el)
    forM_ (zip exprs typs) (uncurry checkExprTypeValid)
    return retTyp
checkFunCall ident _ _ = throwSemError $ CallingNotFunction ident

checkExprTypeValid :: Expr -> Type -> SemEval ()
checkExprTypeValid expr typ1 = do
    typ2 <- checkExpr expr
    case typ1 of
        TClass (CTClass cls) -> do
            unless (isClassType typ2) (throwSemError $ BadType typ1 typ2)
            let TClass (CTClass cls') = typ2
            b <- isSubclass cls' cls
            unless b (throwSemError $ BadType typ1 typ2)
        _ -> unless (typ1 == typ2) (throwSemError $ BadType typ1 typ2)

checkLvalue :: Expr -> SemEval ()
checkLvalue EVar {} = return ()
checkLvalue EArray {} = return ()
checkLvalue EMemVar {} = return ()
checkLvalue expr = throwSemError $ NotLvalue expr

checkConstBoolExpr :: Expr -> SemEval (Maybe Bool)
checkConstBoolExpr ELitTrue = return $ Just True
checkConstBoolExpr ELitFalse = return $ Just False
checkConstBoolExpr _ = return Nothing

checkItem :: Type -> Env -> Item -> SemEval Env
checkItem typ env (SNoInit ident) = checkVar env typ ident
checkItem typ env (SInit ident expr) = do
    local (const env) (checkExprTypeValid expr typ)
    checkVar env typ ident

checkArg :: Env -> Arg -> SemEval Env
checkArg env (AArg typ ident) = checkVar env typ ident

checkVar :: Env -> Type -> Ident -> SemEval Env
checkVar env typ ident = do
    when (isClassType typ) (do
        let TClass (CTClass name) = typ
        let structs = rStructs env
        unless (Map.member name structs) (throwSemError $ NoSuchClass name))
    when (typ == tVoid) (throwSemError VarVoidType)
    when (Map.member ident (rVars env)) $ do
        let (_, level) = unsafeLookup ident (rVars env)
        when (level == rLev env) (throwSemError $ NameNotUnique ident)
    return $ env {rVars = Map.insert ident (typ, rLev env) (rVars env)}

data SemError = TE ErrorKind Stmt

instance Show SemError where
    show (TE e stm) = format "{0}\nFound at:\n {1}" [show e, printTree stm]

data ErrorKind =
    NoVariable Ident |
    BadType Type Type |
    EqOpError Type Type |
    NotArray Type |
    NotLvalue Expr |
    CallingNotFunction Ident |
    WrongArgsNum Int Int |
    NoMain |
    NameNotUnique Ident |
    NoReturn Ident |
    ReturnNotLast |
    IntLitOutOfRange Integer |
    BadCast Expr |
    BadPlusArgs Type Type |
    NoMember Type Ident |
    VarVoidType |
    BadMain Type |
    NotAClass Type |
    NoSuchClass Ident |
    InheritanceCycle Ident |
    BadInheritance Ident Ident |
    UsingThisOutsideClass

instance Show ErrorKind where
    show (NoVariable (Ident n)) = format "There is no {0} variable" [n]
    show (BadType t1 t2) = format "Wrong type - {1}, {0} expected" [tPrint t1,
                                                                    tPrint t2]
    show (EqOpError t1 t2) = format "Cannot compare {0} with {1}" [tPrint t1,
                                                                   tPrint t2]
    show (NotArray t) = format "Type {0} is not an array type" [tPrint t]
    show (NotLvalue e) = format "Expression {0} is not l-value" [printTree e]
    show (CallingNotFunction (Ident name)) =
        format "{0} is not a function" [name]
    show (WrongArgsNum ci i) = format ("Calling function with {1} arguments" ++
                                       " but {0} required") [show ci, show i]
    show NoMain = "Missing main function"
    show (NameNotUnique (Ident name)) =
        format "{0} is not unique within a scope" [name]
    show (NoReturn (Ident name)) = format "No return statement in function {0}"
                                          [name]
    show ReturnNotLast = "Return statement not last in function"
    show (IntLitOutOfRange int) = format "Integer {0} is out of range"
                                         [show int]
    show (BadCast expr) = format "{0} is not a valid cast" [show expr]
    show (BadPlusArgs typ1 typ2) =
        format "Ints or strings should be arguments of '+', not {0} and {1}"
               [tPrint typ1, tPrint typ2]
    show (NoMember typ (Ident name)) =
        format "Type {0} has no member {1}" [tPrint typ, name]
    show VarVoidType = "Variable sholdn't have a void type"
    show (BadMain typ) =
        format "Main should have signature int(), not {0}" [tPrint typ]
    show (NotAClass typ) = format "Type {0} is not a class type" [tPrint typ]
    show (NoSuchClass (Ident cl)) = format "There is no {0} class" [cl]
    show (InheritanceCycle (Ident cls)) =
        format "There is an inheritance cycle with {0} class." [cls]
    show (BadInheritance (Ident cls) (Ident ident)) =
        format "Bad virtual inheritance for class {0} and member {1}"
               [cls, show ident]
    show UsingThisOutsideClass = "This used outside a class"

tPrint :: Type -> String
tPrint (TBasic BTInt) = "int"
tPrint (TBasic BTStr) = "string"
tPrint (TBasic BTBool) = "boolean"
tPrint (TBasic BTVoid) = "void/null"
tPrint (TArray (ATArray t)) = tPrint t ++ "[]"
tPrint (TClass (CTClass (Ident name))) = name
tPrint (TFun ret types) =
    tPrint ret ++ "(" ++ intercalate ", " (map tPrint types) ++ ")"

throwSemError :: ErrorKind -> SemEval a
throwSemError err = do
    state <- get
    throwError $ TE err (rCurStmt state)

identToType :: Ident -> Type
identToType ident = TClass (CTClass ident)

isFunType :: Type -> Bool
isFunType (TFun _ _) = True
isFunType _ = False

isClassType :: Type -> Bool
isClassType TClass {} = True
isClassType _ = False
