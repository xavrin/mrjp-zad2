module Main where

import Opt(optProg)

import System.IO(stderr, hPrint)
import System.Environment(getArgs, getProgName)
import System.Exit(exitFailure, exitSuccess)
import Control.Monad(unless, when)

import Parser(parse)
import ErrM(Err(Bad, Ok))
import SemCheck(semCheck)
import GenIR(genIR)
import GenAsm(genProg)
import Linking(linkCmd)

import System.Process(callCommand)
import System.FilePath(replaceExtension)
import System.Directory(doesFileExist)

endswith :: String -> String -> Bool
endswith end s = drop (length s - length end) s == end

isFileValid :: String -> IO Bool
isFileValid f = do
    b <- doesFileExist f
    return (b && endswith ".lat" f)

invalidFileMsg :: String
invalidFileMsg = "File name should exist and end with '.lat'."

compileCmd :: FilePath -> FilePath -> String
compileCmd fasm fobj = "nasm -f elf32 -F dwarf -g " ++ fasm ++ " -o " ++ fobj

compile :: FilePath -> Bool -> IO ()
compile f debug = do
    b <- isFileValid f
    unless b (do
        putStrLn "ERROR"
        putStrLn invalidFileMsg
        exitFailure)
    cont <- readFile f
    case parse cont of
        Bad s -> do
            putStrLn "ERROR"
            hPrint stderr s
            exitFailure
        Ok prog ->
            case semCheck prog of
                Left e -> do
                    putStrLn "ERROR"
                    hPrint stderr e
                    exitFailure
                _ -> do
                    putStrLn "OK"
                    let iprog = genIR prog
                    let iprog' = optProg iprog
                    when debug (do
                        print iprog
                        print iprog')
                    let fex = replaceExtension f ""
                    let fasm = replaceExtension f ".s"
                    let fobj = replaceExtension f ".o"
                    let asm = genProg iprog'
                    writeFile fasm (show asm)
                    callCommand $ compileCmd fasm fobj
                    callCommand $ linkCmd fobj fex
                    callCommand $ "rm -f " ++ fobj
                    exitSuccess

main :: IO ()
main = do
    args <- getArgs
    case args of
        ["-D", f] -> compile f True
        [f, "-D"] -> compile f True
        [f] -> compile f False
        _ -> do
            name <- getProgName
            putStrLn $ "Usage: ./" ++ name ++ " <source>" ++ " [-D]"
