module Opt where

import IR
import OptTools
import AbsLatte(Ident(..))
import Misc(unsafeLookup, isValidInt, iDiv, iMod)

import Control.Monad.State(gets, modify, StateT, runStateT)
import Control.Monad.Identity(Identity, runIdentity)
import Control.Monad(forM, forM_, unless)
import Control.Monad.Trans.Reader(ReaderT, runReaderT, local, asks, ask)
import Data.Maybe(catMaybes, fromMaybe)
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set

optProg :: Prog -> Prog
optProg (Prog defs) = let
    optDef (DF typ ident as stmts) = let
        (typ', ident', as', stmts') = optFun (typ, ident, as, stmts)
      in DF typ' ident' as' stmts'
    optDef def = def
  in Prog (map optDef defs)

optFun :: (Type, Ident, [Arg],[Stmt]) -> (Type, Ident, [Arg], [Stmt])
optFun (typ, nm, as, stmts) = let
    gr = getGraph as nm stmts
    f = deletePhi . joinBB . optDead . optPropAndComExprs . optDead . convSSA
    gr' = f gr
  in (typ, nm, rArgs gr', flattenGraph gr')

{- Optimizing both common subexpressions  and copy propagation iteratively -}
optPropAndComExprs :: Graph -> Graph
optPropAndComExprs gr = let
    gr' = optComExpr gr
    gr'' = optProp gr'
  in if gr == gr'' then gr else optPropAndComExprs gr''

{- Code for constant propagation and copy propagation optimizations. -}

data OPropState = OPropState {
    rCVals :: Map.Map Var Val,
    rCVis :: Set.Set Var,
    rCGraph :: Graph,
    rCCur :: Lab,
    rLinfo :: LiveState,
    rLOut :: Set.Set Var
}

type OPropEval a = (StateT OPropState) Identity a

optProp :: Graph -> Graph
optProp gr = let
    defState = OPropState {rCVals = Map.empty,
                           rCVis = Set.empty,
                           rCGraph = gr,
                           rCCur = rBeg gr,
                           rLinfo = genLiveInfo gr,
                           rLOut = Set.empty}
    gr' = fst $ runIdentity (runStateT optPropFixIter defState)
  in if rVerts gr /= rVerts gr' then optProp gr' else gr'

optPropFixIter :: OPropEval Graph
optPropFixIter = do
    modify (\st -> st {rCGraph = joinBB (rCGraph st)})
    modify (\st -> st {rCVis = Set.singleton (rBeg $ rCGraph st),
                       rLinfo = genLiveInfo (rCGraph st)})
    gr <- gets rCGraph
    optPropVisit (rBeg gr)
    modify (\st -> st {rCGraph = delDeadBlocks (rCGraph st)})
    gr' <- gets rCGraph
    if gr == gr' then return gr else optPropFixIter

optPropVisit :: Lab -> OPropEval ()
optPropVisit v = do
    gr <- gets rCGraph
    modify (\st -> st {rCCur = v,
                       rLOut = rOut $ fst $ unsafeLookup v (rLinfo st)})
    mstmts <- forM (unsafeLookup v (rVerts gr)) optPropStmt
    let stmts = catMaybes mstmts
    gr' <- gets rCGraph
    let gr'' = gr' {rVerts = Map.insert v stmts (rVerts gr')}
    modify (\st -> st {rCGraph = gr''})
    let ss = unsafeLookup v (rSuccs gr'')
    forM_ ss (\n -> do
        vis <- gets rCVis
        unless (Set.member n vis) (do
            modify (\st -> st {rCVis = Set.insert n (rCVis st)})
            optPropVisit n))

optPropStmt :: Stmt -> OPropEval (Maybe Stmt)
optPropStmt (SCJmp val lab1 lab2) = do
    val' <- optPropVal val
    cur <- gets rCCur
    case val' of
        (VC (CB True)) -> do
            modify (\st -> st {rCGraph = delEdge cur lab2 (rCGraph st)})
            return $ Just (SJmp lab1)
        (VC (CB False)) -> do
            modify (\st -> st {rCGraph = delEdge cur lab1 (rCGraph st)})
            return $ Just (SJmp lab2)
        v -> return $ Just (SCJmp v lab1 lab2)
optPropStmt stmt@(S4 typ var val1 op val2) = do
    val1' <- optPropVal val1
    val2' <- optPropVal val2
    if val1' == val2' && op == REQ then
        optPropUpdate var (VC $ CB True)
    else case (val1', val2') of
        (VC c1, VC c2) -> optPropOp var c1 op c2 stmt
        (v1, v2) -> return $ Just (S4 typ var v1 op v2)
optPropStmt (SAss typ var val) = do
    val' <- optPropVal val
    out <- gets rLOut
    if not (isConst val') && Set.member var out then
        return $ Just (SAss typ var val')
    else
        optPropUpdate var val'
optPropStmt stmt@(SNot var val) = do
    val' <- optPropVal val
    case val' of
        (VC (CB b)) -> optPropUpdate var (VC $ CB $ not b)
        _ -> return $ Just stmt
optPropStmt (SLoad typ var val) = do
    val' <- optPropVal val
    return $ Just (SLoad typ var val')
optPropStmt (SStore typ val1 val2) = do
    val1' <- optPropVal val1
    val2' <- optPropVal val2
    return $ Just (SStore typ val1' val2')
optPropStmt (SElPtr typ var typ1 val1 val2) = do
    val1' <- optPropVal val1
    val2' <- optPropVal val2
    return $ Just (SElPtr typ var typ1 val1' val2')
optPropStmt (SRet val) = do
    val' <- optPropVal val
    return $ Just (SRet val')
optPropStmt (SCall typ var ident vals) = do
    vals' <- forM vals optPropVal
    return $ Just (SCall typ var ident vals')
optPropStmt (SPhi typ var [(_, val)]) = do
    val' <- optPropVal val
    optPropStmt (SAss typ var val')
optPropStmt (SPhi typ var opts) = do
    let optPropTryOpt (lab, val) = do
        val' <- optPropVal val
        return (lab, val')
    opts' <- forM opts optPropTryOpt
    return $ Just (SPhi typ var opts')
optPropStmt (SAlloc typ var val) = do
    val' <- optPropVal val
    return $ Just (SAlloc typ var val')
optPropStmt stmt = return (Just stmt)

optPropOp :: Var -> Const -> Op -> Const -> Stmt -> OPropEval (Maybe Stmt)
optPropOp var (CI i1) op (CI i2) stmt = do
    let mc = if isRelOp op then Just $ CB $ relOpToFun op i1 i2
                           else arOpSafe i1 op i2
    case mc of
        Just c -> optPropUpdate var (VC c)
        Nothing -> return (Just stmt)
optPropOp var (CB b1) op (CB b2) _ = do
    let c = CB $ eqOpToFun op b1 b2
    optPropUpdate var (VC c)
optPropOp var CNull op CNull _ = do
    let c = CB $ eqOpToFun op CNull CNull
    optPropUpdate var (VC c)
optPropOp _ _ _ _ _ = error "Internal error: bad combination of types"

optPropUpdate :: Var -> Val -> OPropEval(Maybe Stmt)
optPropUpdate var val = do
    modify (\st -> st {rCVals = Map.insert var val (rCVals st)})
    return Nothing

arOpSafe :: Integer -> Op -> Integer -> Maybe Const
arOpSafe i1 APlus i2 = if isValidInt (i1 + i2) then Just (CI $ i1 + i2)
                                               else Nothing
arOpSafe i1 AMinus i2 = if isValidInt (i1 - i2) then Just (CI $ i1 - i2)
                                                else Nothing
arOpSafe i1 ATimes i2 = if isValidInt (i1 * i2) then Just (CI $ i1 * i2)
                                                else Nothing
arOpSafe i1 ADiv i2 =
    if i2 /= 0 && isValidInt (i1 `iDiv` i2) then Just (CI $ i1 `iDiv` i2)
                                            else Nothing
arOpSafe i1 AMod i2 =
    if i2 /= 0 && isValidInt (i1 `iMod` i2) then Just (CI $ i1 `iMod` i2)
                                            else Nothing
arOpSafe _ _ _ = error "Internal error: arOpSafe"

relOpToFun :: Ord a => Op -> a -> a -> Bool
relOpToFun RGT x y = x > y
relOpToFun RLT x y = x < y
relOpToFun REQ x y = x == y
relOpToFun RNE x y = x /= y
relOpToFun RGE x y = x >= y
relOpToFun RLE x y = x <= y
relOpToFun _ _ _ = error "Internal error: Invalid relOp"

eqOpToFun :: Eq a => Op -> a -> a -> Bool
eqOpToFun REQ x y = x == y
eqOpToFun RNE x y = x /= y
eqOpToFun _ _ _ = error "Internale error: Invalid eqOp"

optPropVal :: Val -> OPropEval Val
optPropVal val@(VC _) = return val
optPropVal val@(VI var) = do
    cv <- gets rCVals
    return $ fromMaybe val (Map.lookup var cv)

{- Throwing out the dead code -}

optDead :: Graph -> Graph
optDead gr = let
    linfo = genLiveInfo gr
    mapFun lab = optDeadBlock (rOut $ fst $ unsafeLookup lab linfo)
  in gr {rVerts = Map.mapWithKey mapFun (rVerts gr)}

optDeadBlock :: Set.Set Var -> [Stmt] -> [Stmt]
optDeadBlock out stmts = let
    foldFun stmt (live, lmstmt) = let
        (live', mstmt) = optDeadStmt live stmt
      in (live', mstmt : lmstmt)
  in catMaybes $ snd $ foldr foldFun (out, []) stmts

optDeadStmt :: Set.Set Var -> Stmt -> (Set.Set Var, Maybe Stmt)
optDeadStmt out stmt@(SCall _ var _ vals) = (genInSet out [var] vals, Just stmt)
optDeadStmt out stmt@(SAlloc _ var val) = (genInSet out [var] [val], Just stmt)
optDeadStmt out stmt@SVRet = (out, Just stmt)
optDeadStmt _ stmt@(SRet val) = (valsToUseSet [val], Just stmt)
optDeadStmt out stmt@(SElPtr _ var _ val1 val2) =
    genericOptDeadStmt out stmt var [val1, val2]
optDeadStmt out stmt@(SStore _ val1 val2) = (genInSet out [] [val1, val2],
                                             Just stmt)
optDeadStmt out stmt@(SLoad _ var val) = (genInSet out [var] [val], Just stmt)
optDeadStmt out stmt@(SNot var val) = genericOptDeadStmt out stmt var [val]
optDeadStmt out stmt@(SAss _ var val) = genericOptDeadStmt out stmt var [val]
optDeadStmt out stmt@(S4 _ var val1 AMod val2) =
    if nonZero val2 then genericOptDeadStmt out stmt var [val1, val2]
                    else (genInSet out [var] [val1, val2], Just stmt)
optDeadStmt out stmt@(S4 _ var val1 ADiv val2) =
    if nonZero val2 then genericOptDeadStmt out stmt var [val1, val2]
                    else (genInSet out [var] [val1, val2], Just stmt)
optDeadStmt out stmt@(S4 _ var val1 _ val2) =
    genericOptDeadStmt out stmt var [val1, val2]
optDeadStmt out stmt@(SCJmp val _ _) = (genInSet out [] [val], Just stmt)
optDeadStmt out stmt@(SPhi _ var opts) =
    (genInSet out [var] (map snd opts), Just stmt)
optDeadStmt out stmt = (out, Just stmt)

genericOptDeadStmt :: Set.Set Var -> Stmt -> Var -> [Val] -> (Set.Set Var,
                                                              Maybe Stmt)
genericOptDeadStmt out stmt var vals =
    if Set.member var out then (genInSet out [var] vals, Just stmt)
                          else (out, Nothing)

nonZero :: Val -> Bool
nonZero VI {} = False
nonZero (VC (CI i)) = i /= 0
nonZero _ = error "Internal error: nonZero"

{- Here begins the common subexpression optimization. We will do local
   subexpression elimination and global with a restriction - we will notice
   only blocks that dominate us. -}

{- A data structure for overloading comparison operator for statements.
   We must ignore the name of variable begin assigned.
   Important we only care about S4, SNot, SElPtr and SPhi.
   We rely on the fact that the constructor is the most significant factor
   of comparing and the arguments are taken into consideration later in the
   derived Ord in Stmt.
   Important: it should be only run for SSA form!!! Comparison function would
   fail in case of multiple vars' definitions. -}

data EStmt = ESt Stmt

eStmtLE :: EStmt -> EStmt -> Bool
(ESt (S4 typ _ val1 op val2)) `eStmtLE` (ESt (S4 typ' _ val1' op' val2')) =
    S4 typ cId val1 op val2 <= S4 typ' cId val1' op' val2'
(ESt (SNot _ val)) `eStmtLE` (ESt (SNot _ val')) =
    SNot cId val <= SNot cId val'
(ESt (SElPtr typ _ typ1 val1 val2)) `eStmtLE` (ESt (SElPtr typ' _ typ1' val1' val2')) =
    SElPtr typ cId typ1 val1 val2 <= SElPtr typ' cId typ1' val1' val2'
(ESt (SPhi typ _ opts)) `eStmtLE` (ESt (SPhi typ' _ opts')) =
    SPhi typ cId opts <= SPhi typ' cId opts'
(ESt stmt) `eStmtLE` (ESt stmt') = stmt <= stmt'

instance Eq EStmt where
    es1 == es2 = es1 `eStmtLE` es2 && es2 `eStmtLE` es1

instance Ord EStmt where
    es1 <= es2 = es1 `eStmtLE` es2

cId :: Ident
cId = Ident "x"

data OCExprState = OCExprState {
    eGraph :: Graph
}

data OCExprEnv = OCExprEnv {
    eExprs :: Map.Map EStmt Var
}

type ComExprEval a = ReaderT OCExprEnv (StateT OCExprState Identity) a

optComExpr :: Graph -> Graph
optComExpr gr = let
    defState = OCExprState {eGraph = gr}
    defEnv = OCExprEnv {eExprs = Map.empty}
    m = optComExprNode (genDomTree gr)
  in eGraph $ snd $ runIdentity (runStateT (runReaderT m defEnv) defState)

optComExprNode :: DTree -> ComExprEval ()
optComExprNode (DTree v chs) = do
    gr <- gets eGraph
    let stmts = unsafeLookup v (rVerts gr)
    (stmts', env) <- optComExprStmts stmts
    gr' <- gets eGraph
    modify (\st -> st {eGraph = gr' {rVerts = Map.insert v stmts' (rVerts gr')}})
    forM_ chs (local (const env) . optComExprNode)

optComExprStmts :: [Stmt] -> ComExprEval ([Stmt], OCExprEnv)
optComExprStmts (stmt : stmts) = do
    (stmt', env) <- optComExprStmt stmt
    (stmts', env') <- local (const env) (optComExprStmts stmts)
    return (stmt' : stmts', env')
optComExprStmts [] = do
    env <- ask
    return ([], env)

optComExprStmt :: Stmt -> ComExprEval (Stmt, OCExprEnv)
{- We can safely ignore the fact that dividing by zero causes side effects
   because we only deleted the next dividings, crash would already occur.
   We ignore memory accesses because it would require more careful analysis. -}
optComExprStmt stmt@(S4 typ var _ _ _) = optComExprStmtGeneric var typ stmt
optComExprStmt stmt@(SNot var _) = optComExprStmtGeneric var TBool stmt
optComExprStmt stmt@(SElPtr typ var _ _ _) = optComExprStmtGeneric var typ stmt
optComExprStmt stmt@(SPhi typ var _) = optComExprStmtGeneric var typ stmt
optComExprStmt stmt = do
    env <- ask
    return (stmt, env)

optComExprStmtGeneric :: Var -> Type -> Stmt -> ComExprEval (Stmt, OCExprEnv)
optComExprStmtGeneric var typ stmt = do
    exprs <- asks eExprs
    env <- ask
    case Map.lookup (ESt stmt) exprs of
        Nothing ->
            return (stmt, env {eExprs = Map.insert (ESt stmt) var (eExprs env)})
        Just var' -> return (SAss typ var (VI var'), env)
