module OptTools where

import IR
import AbsLatte(Ident(..))
import Misc(unsafeLookup, constMap, mapMap)

import Control.Monad.State(gets, modify, StateT, runStateT)
import Control.Monad.Identity(Identity, runIdentity)
import Control.Monad(foldM, forM, forM_, unless)
import Control.Monad.Trans.Reader(ReaderT, runReaderT, local, asks, ask)
import Data.Maybe(fromMaybe, mapMaybe)
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import qualified Data.List as List
import Control.Exception.Base(assert)

type TypeInfo = Map.Map Var Type

data Graph = Graph {
    rBeg :: Lab,
    rVerts :: Map.Map Lab [Stmt],
    rSuccs :: Map.Map Lab [Lab],
    rPrevs :: Map.Map Lab [Lab],
    rArgs :: [Arg],
    rTypes :: TypeInfo,
    rOrder :: [Lab]
} deriving (Eq, Show)

getGraph :: [Arg] -> Lab -> [Stmt] -> Graph
getGraph args start stmts = let
    mapFun (SLab lab) = Just lab
    mapFun _ = Nothing
    labs = start : mapMaybe mapFun stmts
    gr = fst $ createGraph (Graph {rBeg = start,
                                   rVerts = Map.empty,
                                   rSuccs = Map.empty,
                                   rPrevs = Map.empty,
                                   rArgs = args,
                                   rTypes = calcTypes args stmts,
                                   rOrder = labs}, [])
                           (SLab start : stmts)
    keys = Map.keys (rVerts gr)
  -- We have to give default values in succs and prevs for everyone
    gr' = gr {rSuccs = Map.union (rSuccs gr) (constMap keys []),
              rPrevs = Map.union (rPrevs gr) (constMap keys [])}
  in delDeadBlocks gr'

getStmts :: Lab -> Graph -> [Stmt]
getStmts v gr = if v == rBeg gr then stmts else SLab v : stmts
    where stmts = unsafeLookup v (rVerts gr)

createGraph :: (Graph, [Stmt]) -> [Stmt] -> (Graph, [Stmt])
createGraph (gr, cur) (stmt@(SLab _) : stmts) =
    createGraph (gr, stmt : assert (null cur) []) stmts
createGraph (gr, cur) (stmt@(SJmp lab) : stmts) = let
    gr' = addBlock (reverse (stmt : cur)) [lab] gr
  in createGraph (gr', []) stmts
createGraph (gr, cur) (stmt@(SCJmp _ lab1 lab2) : stmts) = let
    gr' = addBlock (reverse (stmt : cur)) [lab1, lab2] gr
  in createGraph (gr', []) stmts
createGraph (gr, cur) (stmt@SVRet : stmts) = let
    gr' = addBlock (reverse (stmt : cur)) [] gr
  in createGraph (gr', []) stmts
createGraph (gr, cur) (stmt@(SRet _) : stmts) = let
    gr' = addBlock (reverse (stmt : cur)) [] gr
  in createGraph (gr', []) stmts
createGraph (gr, cur) (stmt : stmts) = createGraph (gr, stmt : cur) stmts
{- It means that there is nothing in cur or it is dead code - otherwise
-  semantic control would return error. -}
createGraph (gr, _) [] = (gr, [])

{- Function for handling the case where a block begins without a label.
   The first argument: is a return block.-}
addBlock :: [Stmt] -> [Lab]-> Graph -> Graph
addBlock body@(SLab start : _) neighs gr = let
    gr' = foldr (addEdge start) gr neighs
  in gr' {rVerts = Map.insert start (tail body) (rVerts gr')}
-- simply forgetting about the dead code
addBlock _ _ gr = gr

{- It works during the graph build. If there is no entry for some label, it
   is created. -}
addEdge :: Lab -> Lab -> Graph -> Graph
addEdge lab1 lab2 g = let
    addEdge1 l1 l2 m = let
        Just oldEdges = if Map.member l1 m then Map.lookup l1 m else Just []
      in Map.insert l1 (l2 : oldEdges) m
  in g {rSuccs = addEdge1 lab1 lab2 (rSuccs g),
        rPrevs = addEdge1 lab2 lab1 (rPrevs g)}

{- delEdge assumes that Graph is in a consistent state i.e. every label has
   its entries in all data structures -}
delEdge :: Lab -> Lab -> Graph -> Graph
delEdge lab1 lab2 g = let
    delEdge1 l1 l2 m = Map.insert l1 (List.delete l2 (unsafeLookup l1 m)) m
  in g {rSuccs = delEdge1 lab1 lab2 (rSuccs g),
        rPrevs = delEdge1 lab2 lab1 (rPrevs g)}

{- Deletes unreacheable blocks in the code. Also fixes all phi functions
- (i.e. removes the options that are not possible) -}
delDeadBlocks :: Graph -> Graph
delDeadBlocks gr = let
    start = rBeg gr
    dead = Set.difference (Set.fromList (Map.keys $ rVerts gr))
                          (getReacheable start gr (Set.singleton start))
    mapFun _ = filter (`Set.notMember` dead)
    filterFun k _ = k `Set.notMember` dead
    {- We have to delete dead nodes from verts, prevs and succs.
       Additionally, good nodes can be pointed by dead nodes, so we have to
       filter prevs. -}
    succs' = Map.filterWithKey filterFun (rSuccs gr)
    prevs' = Map.filterWithKey filterFun (rPrevs gr)
    verts' = Map.filterWithKey filterFun (rVerts gr)
    order' = filter (`Set.notMember` dead) (rOrder gr)
    prevs'' = Map.mapWithKey mapFun prevs'
    verts'' = Map.map (delDeadPhi dead) verts'
  in gr {rVerts = verts'', rSuccs = succs', rPrevs = prevs'', rOrder = order'}

delDeadPhi :: Set.Set Lab -> [Stmt] -> [Stmt]
delDeadPhi deadLabs (SPhi typ var opts : stmts) = let
    {- Important! If the resulting opts will be empty the block will be
       removed anyway, so we don't have to worry about that. -}
    filterFun (lab, _) = lab `Set.notMember` deadLabs
    opts' = filter filterFun opts
  in (SPhi typ var opts' : delDeadPhi deadLabs stmts)
delDeadPhi _ stmts = stmts

getReacheable :: Lab -> Graph -> Set.Set Lab -> Set.Set Lab
getReacheable v gr vis = let
    foldFun x s =
        if Set.member x s then s else getReacheable x gr (Set.insert x s)
  in foldr foldFun vis (unsafeLookup v (rSuccs gr))

calcTypes :: [Arg] -> [Stmt] -> TypeInfo
calcTypes as stmts =
    calcTypesFromStmts stmts (calcTypesFromArgs as Map.empty)

calcTypesFromArgs :: [Arg] -> TypeInfo -> TypeInfo
calcTypesFromArgs (Arg var typ : as) tinfo =
    calcTypesFromArgs as (Map.insert var typ tinfo)
calcTypesFromArgs [] tinfo = tinfo

calcTypesFromStmts :: [Stmt] -> TypeInfo -> TypeInfo
calcTypesFromStmts (stmt : stmts) tinfo_ = let
    updateType var typ tinfo =
        case Map.lookup var tinfo of
            Nothing -> Map.insert var typ tinfo
            {- Just typ' -> assert (typ' == typ) tinfo -}
            Just _ -> tinfo
    calcTypesFromStmt (S4 typ var _ _ _) tinfo = updateType var typ tinfo
    calcTypesFromStmt (SAss typ var _) tinfo = updateType var typ tinfo
    calcTypesFromStmt (SNot var _) tinfo = updateType var TBool tinfo
    calcTypesFromStmt (SLoad typ var _) tinfo = updateType var typ tinfo
    calcTypesFromStmt (SElPtr typ var _ _ _) tinfo = updateType var typ tinfo
    calcTypesFromStmt (SAlloc typ var _) tinfo = updateType var typ tinfo
    calcTypesFromStmt (SCall typ var _ _) tinfo = updateType var typ tinfo
    calcTypesFromStmt (SPhi typ var _) tinfo = updateType var typ tinfo
    calcTypesFromStmt _ tinfo = tinfo
  in calcTypesFromStmts stmts (calcTypesFromStmt stmt tinfo_)
calcTypesFromStmts [] tinfo = tinfo

{- the first argument is for keeping the order of code -}
flattenGraph :: Graph -> [Stmt]
flattenGraph gr = let
    mapFun v = SLab v : unsafeLookup v (rVerts gr)
  in tail $ concatMap mapFun (rOrder gr)

{- Here begins the liveness analysis -}

data LiveInfo = LiveInfo {
    rIn :: Set.Set Var,
    rOut :: Set.Set Var
} deriving (Show, Eq)

type LiveState = Map.Map Lab (LiveInfo, [LiveInfo])

data DfsState = DfsState {
    rVisited :: Set.Set Lab,
    rGraph :: Graph,
    rState :: LiveState
}

type LiveDfs a = (StateT DfsState) Identity a

-- Generates liveinfo for external needs.
genLiveInfo :: Graph -> LiveState
genLiveInfo gr = let
    lst = _genLiveInfo gr
    mapFun lab (glinfo, linfos) = let
        stmts = getStmts lab gr
      in case stmts of
        (SLab {} : _) -> (glinfo, LiveInfo {rIn = rIn glinfo,
                                            rOut = rIn glinfo} : linfos)
        _ -> (glinfo, linfos)
  in Map.mapWithKey mapFun lst

-- Generates liveinfo for internal needs. Ignores SLab stmt.
_genLiveInfo :: Graph -> LiveState
_genLiveInfo gr = let
    lab = rBeg gr
    m = liveFindFixPoint lab
    startState = DfsState {rVisited = Set.singleton lab,
                           rGraph = gr,
                           rState = createBegLiveState gr}
  in rState $ snd $ runIdentity (runStateT m startState)

createBegLiveState :: Graph -> LiveState
createBegLiveState gr = let
    keys = Map.keys (rVerts gr)
    vals = replicate (length keys)
                     (LiveInfo {rIn = Set.empty, rOut = Set.empty}, [])
  in Map.fromList (zip keys vals)

liveFindFixPoint :: Lab -> LiveDfs ()
liveFindFixPoint lab = do
    st <- gets rState
    modify (\st' -> st' {rVisited = Set.singleton lab})
    liveDfsVisit lab
    st' <- gets rState
    unless (st == st') (liveFindFixPoint lab)

liveDfsVisit :: Lab -> LiveDfs ()
liveDfsVisit node = do
    gr <- gets rGraph
    let ns = unsafeLookup node (rSuccs gr)
    let iter n = do
        vis <- gets rVisited
        unless (Set.member n vis) (do
            modify (\st -> st {rVisited = Set.insert n (rVisited st)})
            liveDfsVisit n)
        lst <- gets rState
        let (nodeInfo, stmtsInfo) = unsafeLookup node lst
        let (neighInfo, _) = unsafeLookup n lst
        let nodeInfo' = nodeInfo {rOut = Set.union (rOut nodeInfo)
                                                     (rIn neighInfo)}
        modify (\st -> st {rState = Map.insert node (nodeInfo', stmtsInfo)
                                              (rState st)})
    forM_ ns iter
    lst <- gets rState
    let foldFun (lout', linfo) stmt = let
                info = liveBlockVisit lout' stmt
            in return (rIn info, info : linfo)
    let nodeOut = rOut $ fst (unsafeLookup node lst)
    (_, stmtsInfo) <- foldM foldFun (nodeOut, [])
                            (reverse (unsafeLookup node (rVerts gr)))
    let lst' = Map.insert node (LiveInfo {rIn = rIn $ head stmtsInfo,
                                          rOut = nodeOut},
                                stmtsInfo) lst
    modify (\st -> st {rState = lst'})

liveBlockVisit :: Set.Set Var -> Stmt -> LiveInfo
liveBlockVisit out (SCall _ var _ vals) = genericBlockVisit out [var] vals
liveBlockVisit out (SAlloc _ var val) = genericBlockVisit out [var] [val]
liveBlockVisit out SVRet = LiveInfo {rIn = assert (Set.null out) out,
                                     rOut = out}
liveBlockVisit out (SRet val) = LiveInfo {rIn = valsToUseSet [val],
                                          rOut = assert (Set.null out) out}
liveBlockVisit out (SElPtr _ var _ val1 val2) = genericBlockVisit out [var]
                                                                  [val1, val2]
liveBlockVisit out (SStore _ val1 val2) = genericBlockVisit out [] [val1, val2]
liveBlockVisit out (SLoad _ var val) = genericBlockVisit out [var] [val]
liveBlockVisit out (SNot var val) = genericBlockVisit out [var] [val]
liveBlockVisit out (SAss _ var val) = genericBlockVisit out [var] [val]
liveBlockVisit out (S4 _ var val1 _ val2) = genericBlockVisit out [var] [val1,
                                                                         val2]
liveBlockVisit out (SCJmp val _ _) = genericBlockVisit out [] [val]
liveBlockVisit out (SPhi _ var opts) =
    genericBlockVisit out [var] (map snd opts)
liveBlockVisit out _ = genericBlockVisit out [] []

genInSet :: Set.Set Var -> [Var] -> [Val] -> Set.Set Var
genInSet out vars vals = let
    use = valsToUseSet vals
    kill = Set.fromList vars
  in Set.union (Set.difference out kill) use

genericBlockVisit :: Set.Set Var -> [Var] -> [Val] -> LiveInfo
genericBlockVisit out vars vals = let
  in LiveInfo {rIn = genInSet out vars vals , rOut = out}

valsToUseSet :: [Val] -> Set.Set Var
valsToUseSet vals = let
    fun (VI var) acc = Set.insert var acc
    fun VC {} acc = acc
  in foldr fun Set.empty vals

{- Here begins the code for generating the dominator tree needed for SSA
-  conversion -}

type DomState = Map.Map Lab (Set.Set Lab)

data DomDfsState = DomDfsState {
    rDVisited :: Set.Set Lab,
    rDGraph :: Graph,
    rDState :: DomState
}

type DomDfs a = (StateT DomDfsState) Identity a

genDomSets :: Graph -> DomState
genDomSets gr = let
    lab = rBeg gr
    m = domFindFixPoint lab
    keys = Map.keys (rVerts gr)
    vals = map (\key -> if key == lab then Set.singleton lab
                                      else Set.fromList keys) keys
    begState = Map.fromList (zip keys vals)
    startState = DomDfsState {rDVisited = Set.singleton lab,
                              rDGraph = gr,
                              rDState = begState}
    sets = rDState $ snd $ runIdentity (runStateT m startState)
  in Map.mapWithKey (\key set -> Set.difference set (Set.singleton key)) sets

domFindFixPoint :: Lab -> DomDfs ()
domFindFixPoint lab = do
    st <- gets rDState
    modify (\st' -> st' {rDVisited = Set.singleton lab})
    domDfsIter
    st' <- gets rDState
    unless (st == st') (domFindFixPoint lab)

domDfsIter :: DomDfs ()
domDfsIter = do
    gr <- gets rDGraph
    forM_ (Map.keys (rVerts gr)) (\n -> do
        vis <- gets rDVisited
        unless (Set.member n vis) (domDfsVisit n))

domDfsVisit :: Lab -> DomDfs ()
domDfsVisit node = do
    gr <- gets rDGraph
    let ps = unsafeLookup node (rPrevs gr)
    let iter n = do
        vis <- gets rDVisited
        unless (Set.member n vis) (do
            modify (\st -> st {rDVisited = Set.insert n (rDVisited st)})
            domDfsVisit n)
    forM_ ps iter
    prevSets <- forM ps (\n -> do
        st <- gets rDState
        return $ unsafeLookup n st)
    let begSet = Set.fromList (Map.keys (rVerts gr))
    let nSet = foldr Set.intersection begSet prevSets
    modify (\st -> st {rDState = Map.insert node nSet (rDState st)})

{- reverse the direction of dominance - for every vertex calculate the vertices
   it dominates -}
convDomState :: DomState -> DomState
convDomState mdoms = let
    rmdoms = Map.fromListWith (++) [(v, [k]) | (k, vs) <- Map.toList mdoms,
                                               v <- Set.toList vs]
    -- for labels with no dominated nodes we have to add empty list
    rmdoms' = Map.union rmdoms (constMap (Map.keys mdoms) [])
  in Map.map Set.fromList rmdoms'

data DTree = DTree Lab [DTree] deriving Show

genDomTree :: Graph -> DTree
genDomTree gr = let
    mdoms = genDomSets gr
    vs = Map.keys (rVerts gr)
    calcNewHeight mh v = let
        doms = Set.toList (unsafeLookup v mdoms)
      in 1 + foldr (\el acc -> max (unsafeLookup el mh) acc) 0 doms
    iter mh = let
        mh' = Map.fromList (zip vs (map (calcNewHeight mh) vs))
      in if mh' == mh then mh else iter mh'
    mheight :: Map.Map Lab Integer -- otherwise there are annoying warnings
    mheight = iter (constMap vs 0)
    -- now we have to calculate the sets
    rmdoms = convDomState mdoms
    -- there will be an edge iff we dominate someone and height diff is 1
    createTree v = let
        vHeight = unsafeLookup v mheight
        ldom = Set.toList (unsafeLookup v rmdoms)
        lidom = filter (\n -> vHeight + 1 == unsafeLookup n mheight) ldom
      in DTree v (map createTree lidom)
  in createTree (rBeg gr)

{- Now we'll take care of inserting phi functions -}

{- Code for renaming variables after inserting phi functions -}
data RenameEnv = RenameEnv {
    rPVars :: Map.Map Var Var
}

data RenameState = RenameState {
    rPNextTmp :: Integer,
    rPGraph :: Graph
}

type RenameEval a = ReaderT RenameEnv (StateT RenameState Identity) a

renameVars :: Graph -> DTree -> Graph
renameVars gr tr = let
    renameFun tree = do
        env <- renameArgs
        local (const env) (renameNode tree)
    m = renameFun tr
    defState = RenameState {rPNextTmp = 0, rPGraph = gr}
    defEnv = RenameEnv {rPVars = Map.empty}
  in rPGraph $ snd $ runIdentity (runStateT (runReaderT m defEnv) defState)

renameArgs :: RenameEval RenameEnv
renameArgs = do
    let renameArg (vs, l) (Arg var typ) = do
        tmp <- nextTmp
        return (Map.insert var tmp vs, Arg tmp typ : l)
    vs <- asks rPVars
    gr <- gets rPGraph
    (vs', as) <- foldM renameArg (vs, []) (rArgs gr)
    modify (\st -> st {rPGraph = gr {rArgs = reverse as}})
    return RenameEnv {rPVars = vs'}

renameNode :: DTree -> RenameEval ()
renameNode (DTree v chs) = do
    gr <- gets rPGraph
    (stmts', env) <- renameStmts (unsafeLookup v (rVerts gr))
    modify (\st -> st {rPGraph = gr {rVerts = Map.insert v stmts' (rVerts gr)}})
    let ss = unsafeLookup v (rSuccs gr)
    let fixNeighbour n = do
        -- Very important!!! Assumption that phi is only at the beginning!
        gr' <- gets rPGraph
        nstmts <- fixPhiStmts v (unsafeLookup n (rVerts gr'))
        modify (\st -> st {rPGraph = gr' {rVerts = Map.insert n nstmts (rVerts gr')}})
    forM_ ss (local (const env) . fixNeighbour)
    forM_ chs (local (const env) . renameNode)

fixPhiStmts :: Lab -> [Stmt] -> RenameEval [Stmt]
fixPhiStmts par (SPhi typ var opts : stmts) = do
    opts' <- fixPhi par opts
    stmts' <- fixPhiStmts par stmts
    return (SPhi typ var opts' : stmts')
fixPhiStmts _ stmts = return stmts

fixPhi :: Lab -> [(Lab, Val)] -> RenameEval [(Lab, Val)]
fixPhi par opts_ = let
    fixPhiIter (opt@(lab, val) : opts) = do
        opts' <- fixPhiIter opts
        if lab == par then do
            val' <- renameVal val
            return ((lab, val') : opts')
        else return (opt : opts')
    fixPhiIter [] = return []
  in fixPhiIter opts_

renameStmts :: [Stmt] -> RenameEval ([Stmt], RenameEnv)
renameStmts (stmt : stmts) = do
    (stmt', env') <- renameStmt stmt
    (stmts', env'') <- local (const env') (renameStmts stmts)
    return (stmt' : stmts', env'')
renameStmts [] = do
    env <- ask
    return ([], env)

renameStmt :: Stmt -> RenameEval (Stmt, RenameEnv)
renameStmt (S4 typ var val1 op val2) = do
    val2' <- renameVal val2
    val1' <- renameVal val1
    tmp <- nextTmp
    renameStmtGeneric (S4 typ tmp val1' op val2') var tmp
renameStmt (SAss typ var val) = do
    val' <- renameVal val
    tmp <- nextTmp
    renameStmtGeneric (SAss typ tmp val') var tmp
renameStmt (SNot var val) = do
    val' <- renameVal val
    tmp <- nextTmp
    renameStmtGeneric (SNot tmp val') var tmp
renameStmt (SLoad typ var val) = do
    val' <- renameVal val
    tmp <- nextTmp
    renameStmtGeneric (SLoad typ tmp val') var tmp
renameStmt (SStore typ val1 val2) = do
    val1' <- renameVal val1
    val2' <- renameVal val2
    env <- ask
    return (SStore typ val1' val2', env)
renameStmt (SElPtr typ var typ1 val1 val2) = do
    val1' <- renameVal val1
    val2' <- renameVal val2
    tmp <- nextTmp
    renameStmtGeneric (SElPtr typ tmp typ1 val1' val2') var tmp
renameStmt (SRet val) = do
    val' <- renameVal val
    env <- ask
    return (SRet val', env)
renameStmt (SAlloc typ var val) = do
    val' <- renameVal val
    tmp <- nextTmp
    renameStmtGeneric (SAlloc typ tmp val') var tmp
renameStmt (SCall typ var name vals) = do
    vals' <- forM vals renameVal
    tmp <- nextTmp
    renameStmtGeneric (SCall typ tmp name vals') var tmp
renameStmt (SPhi typ var opts) = do
    tmp <- nextTmp
    renameStmtGeneric (SPhi typ tmp opts) var tmp
renameStmt (SCJmp val lab1 lab2) = do
    val' <- renameVal val
    env <- ask
    return (SCJmp val' lab1 lab2, env)
renameStmt stmt = do
    env <- ask
    return (stmt, env)

renameStmtGeneric :: Stmt -> Var -> Var -> RenameEval (Stmt, RenameEnv)
renameStmtGeneric stmt var var' = do
    env <- ask
    return (stmt, env {rPVars = Map.insert var var' (rPVars env)})

renameVal :: Val -> RenameEval Val
renameVal val@VC {} = return val
renameVal (VI var) = do
    -- since there are string literals sometimes there will be no identifier
    vs <- asks rPVars
    return $ VI $ fromMaybe var (Map.lookup var vs)

nextTmp :: RenameEval Var
nextTmp = do
    i <- gets rPNextTmp
    modify (\st -> st {rPNextTmp = rPNextTmp st + 1})
    return $ Ident $ "p" ++ show i

{- Here is the code responsible for inserting phi function.
   At this moment the algorithm is: insert phi function for live variables at
   the beginning of each block. -}
insertPhi :: Graph -> Graph
insertPhi gr = let
    b = rBeg gr
    lst = _genLiveInfo gr
    foldFun n gr' = let
        stmts = unsafeLookup n (rVerts gr')
        liveIn = Set.toList $ rIn $ fst $ unsafeLookup n lst
        createPhi var = let
            ps = unsafeLookup n (rPrevs gr')
            typ = unsafeLookup var (rTypes gr')
            stmt = SPhi typ var (zip ps (replicate (length ps) (VI var)))
          in [stmt | Map.member var (rTypes gr')]
        stmts' = concatMap createPhi liveIn ++ stmts
      in gr' {rVerts = Map.insert n stmts' (rVerts gr')}
    nonStartNodesSet = Set.difference (Set.fromList (Map.keys (rVerts gr)))
                                      (Set.singleton b)
  in foldr foldFun gr (Set.toList nonStartNodesSet)

deletePhi :: Graph -> Graph
deletePhi gr = let
    deletePhiStmts (SPhi typ var opts : stmts) mstmt =
        deletePhiStmts stmts (deletePhiStmt var typ opts mstmt)
    deletePhiStmts _ mstmt = mstmt
    deletePhiStmt var typ ((lab, val) : opts) mstmt = let
        stmts = unsafeLookup lab mstmt
        mstmt' = Map.insert lab (SAss typ var val : stmts) mstmt
      in deletePhiStmt var typ opts mstmt'
    deletePhiStmt _ _ [] mstmt = mstmt
    foldFun n = deletePhiStmts (unsafeLookup n (rVerts gr))
    keys = Map.keys $ rVerts gr
    mstmt_ = foldr foldFun (constMap keys []) keys
    mapFun l1 l2 = let
        {- using assumption that every block ends with jump instruction or
           return; in case of return the right argument will be empty -}
        (jmp : rl1) = reverse l1
        filterFun SPhi {} = False
        filterFun _ = True
      in filter filterFun (reverse (jmp : (l2 ++ rl1)))
  in gr {rVerts = Map.unionWith mapFun (rVerts gr) mstmt_}

convSSA :: Graph -> Graph
convSSA gr = let
    gr' = insertPhi gr
    tr = genDomTree gr'
    gr'' = renameVars gr' tr
    stmts = flattenGraph gr''
  in gr'' {rTypes = calcTypes (rArgs gr'') stmts}

{- Code for joining basic blocks in case of chains. -}

type Names = Map.Map Lab Lab

joinBB :: Graph -> Graph
joinBB gr_ = let
    isDeleted v nm' = v /= joinBBNaiveLookup v nm'
    isChainEnd g v = length (unsafeLookup v (rSuccs g)) /= 1
    vs = map fst (Map.toList (rVerts gr_))
    chainEnds = filter (isChainEnd gr_) vs
    {- We will only start it for the ends of chains. It will make life much
       easier. -}
    (gr, nm_) = foldr joinBBOne (gr_, joinBBInit vs) chainEnds
    nm = shortenPaths nm_
    {- We have to throw deleted nodes out of verts, prevs and succs. Also, we
       need to remap entries in prevs and also change some phi statements -}
    filterFun2 k _ = not (k `isDeleted` nm)
    verts' = Map.filterWithKey filterFun2 (rVerts gr)
    prevs' = Map.filterWithKey filterFun2 (rPrevs gr)
    succs' = Map.filterWithKey filterFun2 (rSuccs gr)
    order' = filter (not . (`isDeleted` nm)) (rOrder gr)
    verts'' = Map.map (remapPhi nm) verts'
    prevs'' = Map.map (map (`joinBBNaiveLookup` nm)) prevs'
    gr' = gr {rVerts = verts'', rPrevs = prevs'', rSuccs = succs', rOrder = order'}
  in gr'

remapPhi :: Names -> [Stmt] -> [Stmt]
remapPhi nm (SPhi typ var opts : stmts) = let
    opts' = map (\(lab, val) -> (joinBBNaiveLookup lab nm, val)) opts
    in (SPhi typ var opts' : remapPhi nm stmts)
remapPhi _ stmts = stmts

{- Assumption: v is the chain end!! -}
joinBBOne :: Lab -> (Graph, Names) -> (Graph, Names)
joinBBOne v (gr, nm) = let
    filterFun SJmp {} = False
    filterFun SCJmp {} = error "Internal error: joinBBOne"
    filterFun _ = True
  in case Map.lookup v (rPrevs gr) of
    Just [p] ->
        if p == v then
            (gr, nm)
        else case Map.lookup p (rSuccs gr) of
            Just [_] -> let
                nm' = joinBBInsert v p nm
                pStmts = filter filterFun (unsafeLookup p (rVerts gr))
                vStmts = unsafeLookup v (rVerts gr)
                verts' = Map.insert p (pStmts ++ vStmts) (rVerts gr)
                -- We have to move the succs from v to p
                succs' = Map.insert p (unsafeLookup v (rSuccs gr)) (rSuccs gr)
                gr' = gr {rVerts = verts', rSuccs = succs'}
              in joinBBOne p (gr', nm')
            _ -> (gr, nm)
    _ -> (gr, nm)

{- Labels lookup data structure based on find-union algorithm -}

{- We want to shorten the paths in the find-union structure to be able
    to use it later conveniently and efficiently -}
shortenPaths :: Names -> Names
shortenPaths nm = let
    lookup_ lab nm' = snd $ joinBBLookup lab nm'
  in foldr lookup_ nm (Map.keys nm)

joinBBInit :: [Lab] -> Names
joinBBInit = mapMap id

joinBBNaiveLookup :: Lab -> Names -> Lab
joinBBNaiveLookup = unsafeLookup

joinBBLookup :: Lab -> Names -> (Lab, Names)
joinBBLookup lab_ nm_ = let
    mlookup :: Lab -> Names -> (Lab, Names)
    mlookup lab nm =
        if lab == lab' then
            (lab, nm)
        else let
            (lab'', nm') = mlookup lab' nm
          in (lab'', Map.insert lab lab'' nm')
        where lab' = unsafeLookup lab nm
  in mlookup lab_ nm_

joinBBInsert :: Lab -> Lab -> Names -> Names
joinBBInsert = Map.insert

nextUse :: Var -> [Stmt] -> Integer
nextUse var_ stmts_ = let
    nextUseRec var (stmt : stmts) num =
        if isUsedIn (VI var) stmt then num else nextUseRec var stmts (num + 1)
    nextUseRec _ [] num = num
  in nextUseRec var_ stmts_ 0

isUsedIn :: Val -> Stmt -> Bool
isUsedIn val1 (SCJmp val2 _ _) = val1 == val2
isUsedIn val1 (S4 _ _ val2 _ val3) = val1 `elem` [val2, val3]
isUsedIn val1 (SAss _ _ val2) = val1 == val2
isUsedIn val1 (SNot _ val2) = val1 == val2
isUsedIn val1 (SLoad _ _ val2) = val1 == val2
isUsedIn val1 (SStore _ val2 val3) = val1 `elem` [val2, val3]
isUsedIn val1 (SElPtr _ _ _ val2 val3) = val1 `elem` [val2, val3]
isUsedIn val1 (SRet val2) = val1 == val2
isUsedIn val1 (SAlloc _ _ val2) = val1 == val2
isUsedIn val1 (SCall _ _ _ vals) = val1 `elem` vals
isUsedIn val1 (SPhi _ _ opts) =  val1 `elem` map snd opts
isUsedIn _ _ = False
