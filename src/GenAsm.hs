module GenAsm where

import IR
import Asm
import AbsLatte(Ident(..))
import Misc(predefinedFuns, unsafeLookup, constMap, exitFunc, mallocFunc)
import qualified OptTools as Opt

import Control.Monad.State(gets, modify, StateT, runStateT, get)
import Control.Monad.Identity(Identity, runIdentity)
import Control.Monad(forM_, when, unless, forM)
import Control.Exception.Base(assert)

import qualified Data.Maybe as Maybe
import qualified Data.List as List
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set

type Structs = Map.Map Ident [Type]

genProg :: Prog -> Module
genProg (Prog topdefs) = let
    lits = foldr (flip genLit) [] topdefs
    lVars = Set.fromList $ map (\(Lit var _) -> var) lits
    structs = foldr (flip genStruct) Map.empty topdefs
    funs = foldr (flip (genFun structs lVars)) [] topdefs
    predF = mallocFunc : (exitFunc : map fst predefinedFuns)
    fdefs = foldr (\ident l -> FunDecl ident : l) [] predF
    fdefs' = FunGlobal (Ident "_start") : fdefs
  in Module (StrLits lits) (Text (fdefs' ++ funs))

genStruct :: Structs -> TopDef -> Structs
genStruct ms (DSt ident types) = Map.insert ident types ms
genStruct ms _ = ms

genLit :: [StrLit] -> TopDef -> [StrLit]
genLit l (DLit ident str) = Lit ident str : l
genLit l _ = l

genFun :: Structs -> Set.Set Var -> [FunEntry] -> TopDef -> [FunEntry]
genFun sts lits l (DF _ lab@(Ident fname) args_ stmts) = let
    -- Assigning the addresses to arguments
    args = map (\(Arg name _) -> name) args_
    mapArgument (mp, i) var =
        (Map.insert var (AddrRI FP (4 + i * 4)) mp, i + 1)
    gr = Opt.getGraph args_ lab stmts
    linfo = Opt.genLiveInfo gr
    (mapping, _) = foldl mapArgument (Map.empty, firstArg) args
    -- Calculating all the variables that are live-in or live-out at the
    -- ends of basic blocks.
    -- However, we have to dispose of the literals.
    vals = map (\(inf, _) -> Set.union (Opt.rIn inf) (Opt.rOut inf))
               (Map.elems linfo)
    vals' = foldr Set.union Set.empty vals
    vals'' = Set.difference vals' (Set.union lits (Set.fromList args))
    -- Now we can just assign memory locations to these variables.
    mapLocal var (mp, i) = (Map.insert var (AddrRI FP (-4*i)) mp, i + 1)
    {- (mapping', next_loc) = foldr mapLocal (trSh mapping, firstLocal) vals'' -}
    (mapping', nextLoc) = foldr mapLocal (mapping, firstLocal) (Set.toList vals'')
    prologue = funPrologue main nextLoc
    main = fname == "main"
    body = concatMap (\v -> genBBlock sts lits nextLoc v main mapping' linfo gr)
                     (Opt.rOrder gr)
    ident' = Ident $ if main then "_start" else fname
  in FunText ident' (prologue ++ body) : l
genFun _ _ l _ = l

genBBlock :: Structs -> Set.Set Var -> Integer -> Lab -> Bool -> Mapping ->
             Opt.LiveState -> Opt.Graph -> Code
genBBlock structs lits nextLoc v isMain mp lst gr = let
    (vlinfo, slinfo) = unsafeLookup v lst
    out = Set.toList (Set.difference (Opt.rOut vlinfo) lits)
    mConts = Map.fromList $ map (\(v', a) -> (a, Just v')) (Map.toList mp)
    st = State {rStructs = structs,
                rDests = Map.map (\addr -> ([], [addr])) mp,
                rLiveOut = zip out (map (`unsafeLookup` mp) out),
                rRConts = constMap allRegs Nothing,
                rRFree = allRegs,
                rMConts = mConts,
                rMFree = [],
                rNextLoc = nextLoc,
                rOldNextLoc = nextLoc,
                rProtected = Set.empty,
                rIsMain = isMain,
                rCode = [],
                rBlocked = [],
                rRemStmts = [],
                rLits = lits}
    in_ = Set.toList (Set.difference (Opt.rIn vlinfo) lits)
    st' = updateLiveIn in_ mp st
    stmts = Opt.getStmts v gr
    m = genStmts (zip stmts slinfo)
  in rCode $ snd $ runIdentity (runStateT m st')

{- The beginning of a monad. I decided to run the monad only for a basic
   block. -}

data Loc = LMem Addr | LReg Reg

type Code = [Entry]

type Mapping = Map.Map Var Addr

{- A code for allocation of a single basic block. We are given globally
   allocated memory locations for the live-in and live-out variables.
   Very important! We should use only one constructor of addrs with one
   register. It's not one-to-one with real addresses.-}
data State = State {
    rStructs :: Structs,
    {- For every variable - (current memory locations, current register
       locations). -}
    rDests :: Map.Map Var ([Reg], [Addr]),
    {- Live-out variables - their names and locations at wich they should
       be kept at the end of block -}
    rLiveOut :: [(Var, Addr)],
    -- Content of every register.
    rRConts :: Map.Map Reg (Maybe Var),
    -- Free registers
    rRFree :: [Reg],
    -- Content of used memory locations. Useful for saving memory on stack.
    rMConts :: Map.Map Addr (Maybe Var),
    -- Free memory on stack
    rMFree :: [Addr],
    -- Next address to take on stack.
    rNextLoc :: Integer,
    -- The height of stack from the beginning of the block.
    rOldNextLoc :: Integer,
    -- List of variables that currently should be kept.
    rProtected :: Set.Set Var,
    rIsMain :: Bool,
    rCode :: Code,
    -- List of blocked registers in current allocation process.
    rBlocked :: [Reg],
    rRemStmts :: [Stmt],
    -- Global literals
    rLits :: Set.Set Var
} deriving Show

type AsmEval a = (StateT State) Identity a

genStmts :: [(Stmt, Opt.LiveInfo)] -> AsmEval ()
genStmts ((stmt, linfo) : pairs) = do
    let isJmp s = case s of
                      SCJmp {} -> True
                      SJmp {} -> True
                      _ -> False
    modify (\st -> st {rBlocked = [],
                       rProtected = Set.difference (Opt.rOut linfo) (rLits st),
                       rRemStmts = map fst pairs})
    when (isJmp stmt) blockEndCleanup
    genStmt stmt
    let garbage = Set.toList $ Set.difference (Opt.rIn linfo) (Opt.rOut linfo)
    forM_ garbage free
    genStmts pairs
genStmts [] = return ()

genStmt :: Stmt -> AsmEval ()
genStmt (SJmp lab) = emitInstr $ IJmp (DId lab)
genStmt (SCJmp val lab1 lab2) = do
    dest <- allocReg (RAny, Just val)
    emitInstr $ ICmp SDW dest (DConst 1)
    emitInstr $ IJe (DId lab1)
    emitInstr $ IJmp (DId lab2)
genStmt (S4 _ var val1 op val2) =
    if isRelOp op then
        genStmtRel var val1 op val2
    else
        case op of
            APlus -> genStmtArGeneric var val1 val2 (getArInstr op)
            AMinus -> genStmtArGeneric var val1 val2 (getArInstr op)
            ATimes -> genStmtArGeneric var val1 val2 (getArInstr op)
            ADiv -> genStmtDivMod True var val1 val2
            AMod -> genStmtDivMod False var val1 val2
            _ -> error "Internal error: genStmt"
genStmt (SLab lab) = emitLab lab
genStmt (SRet val) = do
    main <- gets rIsMain
    if main then do
        dest <- allocReg (RAny, Just val)
        emitInstr $ IPush SDW dest
        emitInstr $ ICall $ DId exitFunc
    else do
        _ <- allocReg (RSpec EAX, Just val)
        funEpilogue
genStmt SVRet = funEpilogue
genStmt (SAss _ var val) = do
    dest <- allocReg (RReg, Just val)
    -- A small hack - we avoid an additional move instruction - it's
    -- enough that alloc will update the status
    updateDest var dest
genStmt (SNot var val) = do
    [reg, dest] <- allocRegs [(RReg, Just (VC $ CI 1)), (RAny, Just val)]
    emitInstr $ ISub SDW reg dest
    updateDest var reg
genStmt (SCall typ var ident vals) = do
    -- We have to save some registers: eax, ecx and edx. It will be enough
    -- to just force allocating these registers.
    -- We must ensure that no argument will be freed in the alloc call
    -- below!! That's why we'll make a hack.
    [eax, _, _] <- protectVals vals (allocRegs [(RSpec EAX, Nothing),
                                                (RSpec ECX, Nothing),
                                                (RSpec EDX, Nothing)])
    forM_ (reverse vals) (\val -> do
        -- We assume that RAny doesn't cause any operations (especially on
        -- stack. Otherwise it could break things.
        dest <- protectVals vals (allocReg (RAny, Just val))
        emitInstr $ IPush SDW dest)
    emitInstr $ ICall (DId ident)
    unless (null vals) (
        emitInstr $ IAdd SDW (DReg SP) (DConst (4 * toInteger (length vals))))
    -- Saving the result.
    unless (typ == TVoid) (updateDest var eax)
genStmt SPhi {} = error "Internal error: Phi not supported in assembly"
genStmt (SLoad _ var val) = do
    dest@(DReg reg) <- allocReg (RReg, Just val)
    emitInstr $ IMov SDW dest (DMem (AddrR reg))
    updateDest var dest
genStmt (SStore _ val1 val2) = do
    [DReg dstReg, src] <- allocRegs [(RReg, Just val1), (RNoMem, Just val2)]
    emitInstr $ IMov SDW (DMem (AddrR dstReg)) src
genStmt (SElPtr _ var typ val vIdx) = do
    let offset = if isArr typ then 4 else 0
    [dest, dest'] <- allocRegs [(RReg, Just val), (RNoMem, Just vIdx)]
    emitInstr $ ILea dest (calcEffAddress offset dest dest')
    updateDest var dest
genStmt (SAlloc typ var@(Ident vname) val) =
    case typ of
        TArr _ -> do
            let tmp = Ident ("__alloc" ++ vname)
            dest@(DReg reg) <- allocReg (RReg, Just val)
            emitInstr $ ILea dest (AddrRM reg 4 4)
            updateDest tmp dest
            genStmt (SCall typ var mallocFunc [VI tmp])
        TSt st -> do
            sts <- gets rStructs
            let types = unsafeLookup st sts
            let len = toInteger $ 4 * length types
            genStmt (SCall typ var mallocFunc [VC (CI len)])
        _ -> error "Internal error: genStmt salloc"

calcEffAddress :: Integer -> Dest -> Dest -> Addr
calcEffAddress offset (DReg baseReg) (DConst idx) =
    AddrRI baseReg (offset + idx * 4)
calcEffAddress offset (DReg baseReg) (DReg idxReg) =
    AddrRIM baseReg idxReg 4 offset
calcEffAddress _ _ _ = error "Internal error: calcEffAddress"

genStmtRel :: Var -> Val -> Op -> Val -> AsmEval ()
genStmtRel var val1 op val2 = do
    [dest, dest', dest''@(DReg r)] <- allocRegs [(RReg, Just val1),
                                                 (RAny, Just val2),
                                                 (RRegBW, Just $ VC $ CI 0)]
    let bdest = getByteReg r
    emitInstr $ ICmp SDW dest dest'
    emitInstr $ getCmpAss op (DReg bdest)
    updateDest var dest''

{- The result will be kept in the second argument. -}
genStmtArGeneric :: Var -> Val -> Val -> (Dest -> Dest -> Instr) -> AsmEval ()
genStmtArGeneric var val1 val2 f = do
    [dest, dest'] <- allocRegs [(RReg, Just val1), (RAny, Just val2)]
    emitInstr $ f dest dest'
    updateDest var dest

genStmtDivMod :: Bool -> Var -> Val -> Val -> AsmEval ()
genStmtDivMod isDiv var val1 val2 = do
    -- DIV doesn't accept constant dividers
    [eax, edx, dest] <- allocRegs [(RSpec EAX, Just val1),
                                   (RSpec EDX, Nothing),
                                   (RNoConst, Just val2)]
    emitInstr $ IMov SDW edx eax
    emitInstr $ ISar SDW edx (DConst 31)
    -- Now edx contains a sign of eax - a requirement of div
    emitInstr $ IDiv SDW dest
    -- Now eax contains a quotient and edx contains a remainder
    updateDest var (if isDiv then eax else edx)

emitLab :: Lab -> AsmEval ()
emitLab lab = modify (\st -> st {rCode = rCode st ++ [ELab lab]})

emitInstr :: Instr -> AsmEval ()
emitInstr instr = modify (\st -> st {rCode = rCode st ++ [EInstr instr]})

getArInstr :: Op -> Dest -> Dest -> Instr
getArInstr APlus d1 d2 = IAdd SDW d1 d2
getArInstr AMinus d1 d2 = ISub SDW d1 d2
getArInstr ATimes d1 d2 = IMul SDW d1 d2
getArInstr _ _ _ = error "Internal error: getArInstr"

getCmpAss :: Op -> Dest -> Instr
getCmpAss RGT = ISetGT
getCmpAss RLT = ISetLT
getCmpAss REQ = ISetEQ
getCmpAss RNE = ISetNE
getCmpAss RLE = ISetLE
getCmpAss RGE = ISetGE
getCmpAss _ = error "Internal error getCmpAss"

{- Here starts the code for the register allocator. -}

{- A following register allocator will be used:
   It will keep values live at the edges of basic blocks in memory but
   inside basic blocks it will try to use only registers.
   Alloc functions should be called once per statement!!!
   Otherwise a lot of care must be taken to avoid errors. -}

{- Data structure for a request to the register allocator.
   Some operations can break the value stored there, so a copy should be made
   beforehand inside the allocator. It is due to the fact that the result is
   stored into register. Perhaps some more flexible solution could be applied.-}
data Req = RSpec Reg | -- Breaks value.
           RReg | -- Any register, breaks value.
           RAny | -- Register, const or memory, doesn't break value.
           RNoMem | -- Register or const, doesn't break value
           RRegB | -- Byte-sized register, breaks value.
           RRegBW | -- Register with a one-byte part, breaks value.
           RNoConst -- We don't want a bare constant, memory or register only,
                    -- doesn't break value.
           deriving Show

firstLocal :: Integer
firstLocal = 4

firstArg :: Integer
firstArg = 1

funPrologue :: Bool -> Integer -> Code
funPrologue _ nextLoc = let
    code1 = [IPush SDW (DReg FP),
             IMov SDW (DReg FP) (DReg SP)]
    code2 = [IPush SDW (DReg ESI),
             IPush SDW (DReg EDI),
             IPush SDW (DReg EBX)]
    code3 = [ISub SDW (DReg SP) (DConst (4 * (nextLoc - firstLocal)))]
    code4 = code1 ++ code2
    code5 = if nextLoc == firstLocal then code4 else code4 ++ code3
  in map EInstr code5

funEpilogue :: AsmEval ()
funEpilogue = do
    st <- get
    let diff = rOldNextLoc st - firstLocal
    unless (diff == 0) (emitInstr $ IAdd SDW (DReg SP) (DConst $ 4 * diff))
    emitInstr $ IPop SDW (DReg EBX)
    emitInstr $ IPop SDW (DReg EDI)
    emitInstr $ IPop SDW (DReg ESI)
    emitInstr $ IMov SDW (DReg SP) (DReg FP)
    emitInstr $ IPop SDW (DReg FP)
    emitInstr IRet

allocRegs :: [(Req, Maybe Val)] -> AsmEval [Dest]
allocRegs [(RAny, Just (VI var))] = do
    ds <- gets rDests
    case Map.lookup var ds of
        Just (regs, addrs) -> do
            let addrs' = addrs
            return [if null regs then DMem (head (assert (addrs' /= []) addrs))
                                 else DReg (head (assert (regs /= []) regs))]
        -- It's a string literal
        Nothing -> return [DId var]
allocRegs [(RAny, Just (VC c))] = return [DConst (evalConst c)]
allocRegs [(RNoMem, Just (VC c))] = return [DConst (evalConst c)]
allocRegs [(RNoMem, Just (VI var))] = do
    ds <- gets rDests
    case Map.lookup var ds of
        Just (regs, addrs) ->
            if null regs then do
                reg <- chooseReg
                sweepReg reg
                emitInstr $ IMov SDW (DReg reg) (DMem (head addrs))
                return [DReg reg]
            else return [DReg (head regs)]
        -- It's a string literal
        Nothing -> return [DId var]

allocRegs [(RReg, mval@(Just (VI var)))] = do
    ds <- gets rDests
    case Map.lookup var ds of
        Just (regs, _) ->
            if null regs then do
                reg <- chooseReg
                allocRegs [(RSpec reg, mval)]
            else do
                sweepVar var (head regs)
                return [DReg (head regs)]
        _ -> do
            -- It means it's a literal string.
            reg <- chooseReg
            [dest] <- allocRegs [(RSpec reg, Nothing)]
            emitInstr $ IMov SDW (DReg reg) (DId var)
            return [dest]
allocRegs [(RReg, mval)] = do
    reg <- chooseReg
    allocRegs [(RSpec reg, mval)]
allocRegs [(RSpec r, Just (VC c))] = do
    sweepReg r
    emitInstr $ IMov SDW (DReg r) (DConst (evalConst c))
    return [DReg r]
allocRegs [(RSpec r, Nothing)] = do
    sweepReg r
    return [DReg r]
allocRegs [(RSpec r, Just (VI var1))] = do
    ds <- gets rDests
    let (regs, addrs) = unsafeLookup var1 ds
    if r `elem` regs then
        sweepVar var1 r
    else
        if null regs then do
            let addrs' = assert (addrs /= []) addrs
            sweepReg r
            emitInstr $ IMov SDW (DReg r) (DMem (head addrs'))
        else do
            let curReg = head regs
            rc <- gets rRConts
            case unsafeLookup r rc of
                Nothing -> emitInstr $ IMov SDW (DReg r) (DReg curReg)
                Just var2 -> do
                    emitInstr $ IXCHG SDW (DReg r) (DReg curReg)
                    cleanReg var2 r
                    cleanReg var1 curReg
                    insReg var2 curReg
                    insReg var1 r
                    -- There is a chance var1 will be broken.
                    sweepVar var1 r
    return [DReg r]
allocRegs [req1@(RReg, _), req2@(RAny, Just _)] =
    allocRegsGeneric2 req1 req2
allocRegs [req1@(RReg, _), req2@(RReg, Just _)] =
    allocRegsGeneric2 req1 req2
allocRegs [req1@(RReg, _), req2@(RNoMem, Just _)] =
    allocRegsGeneric2 req1 req2
allocRegs [(RSpec EAX, Nothing), (RSpec ECX, Nothing),
           (RSpec EDX, Nothing)] = do
    forM_ [EAX, ECX, EDX] blockReg
    forM_ [EAX, ECX, EDX] sweepReg
    return [DReg EAX, DReg ECX, DReg EDX]
allocRegs [req1@(RReg, Just val1), req2@(RAny, Just val2),
           (RRegBW, Just val3)] = do
    breg <- chooseBReg
    blockReg breg
    [dest3] <- protectVals [val1, val2] (allocRegs [(RSpec breg, Just val3)])
    [dest1] <- protectVals [val2] (allocRegs [req1])
    [dest2] <- allocRegs [req2]
    return [dest1, dest2, dest3]
allocRegs [req1@(RSpec r1, _), req2@(RSpec r2, Nothing),
           (RNoConst, mval3@(Just val3))] = do
    blockReg r1
    blockReg r2
    [[dest1], [dest2]] <- forM [[req1], [req2]] (protectVals [val3] . allocRegs)
    dest3 <- if isConst val3 then do
        r3 <- chooseReg
        [d3] <- allocRegs [(RSpec r3, mval3)]
        return d3
    else do
        [d3] <- allocRegs [(RAny, mval3)]
        return d3
    return [dest1, dest2, dest3]
allocRegs _ = error "Internal error: allocRegs - operation not supported"

allocRegsGeneric2 :: (Req, Maybe Val) -> (Req, Maybe Val) -> AsmEval [Dest]
allocRegsGeneric2 req1 req2@(_, Just val2) = do
    [dest1@(DReg reg)] <- protectVals [val2] (allocRegs [req1])
    blockReg reg
    [dest2] <- allocRegs [req2]
    return [dest1, dest2]
allocRegsGeneric2 _ _ = error "Internal error: allocRegsGeneric2 - operation "
                              "not supported"

-- No side effects!!!!!
chooseReg :: AsmEval Reg
chooseReg = _chooseReg allRegs

chooseBReg :: AsmEval Reg
chooseBReg = _chooseReg [EAX, EBX, ECX, EDX]

_chooseReg :: [Reg] -> AsmEval Reg
_chooseReg regs_ = do
    st <- get
    let regs = regs_ List.\\ rBlocked st
    let fRegs = regs `List.intersect` rRFree st
    if null fRegs then do
        let stmts = rRemStmts st
        let vars = map (\r -> Maybe.fromJust $ r `unsafeLookup` rRConts st) regs
        let uses = map (`Opt.nextUse` stmts) vars
        let maxUse = List.maximum uses
        let Just (reg, _) = List.find (\(_, num) -> num == maxUse)
                                      (zip regs uses)
        return reg
    else return $ head fRegs

sweepReg :: Reg -> AsmEval ()
sweepReg reg = do
    rc <- gets rRConts
    case unsafeLookup reg rc of
        Nothing -> return ()
        Just var -> sweepVar var reg

-- Prevents allocator from freeing the values.
protectVals :: [Val] -> AsmEval a -> AsmEval a
protectVals vals m = do
    let mapFun val = case val of
                         VI v -> Just v
                         _ -> Nothing
    let vars = Maybe.mapMaybe mapFun vals
    prot <- gets rProtected
    modify (\st -> st {rProtected = Set.union prot (Set.fromList vars)})
    res <- m
    modify (\st -> st {rProtected = prot})
    return res

-- It makes a backup of variable if needed. It doesn't break the value held.
sweepVar :: Var -> Reg -> AsmEval ()
sweepVar var curReg = do
    st <- get
    let permRegs = rRFree st List.\\ (curReg : rBlocked st)
    ds <- gets rDests
    let (regs, addrs) = unsafeLookup var ds
    let regs' = filter (`elem` permRegs) regs
    prot <- gets rProtected
    when (null regs' && null addrs && Set.member var prot) (
        if null permRegs then do
            fAddrs <- gets rMFree
            if null fAddrs then do
                nloc <- gets rNextLoc
                emitInstr $ IPush SDW (DReg curReg)
                let addr = AddrRI FP (-4 * nloc)
                insAddr var addr
                modify (\st' -> st' {rNextLoc = nloc + 1})
            else do
                let addr = head fAddrs
                emitInstr $ IMov SDW (DMem addr) (DReg curReg)
                insAddr var addr
        else do
            let reg = head permRegs
            emitInstr $ IMov SDW (DReg reg) (DReg curReg)
            insReg var reg)
    cleanReg var curReg

allocReg :: (Req, Maybe Val) -> AsmEval Dest
allocReg r = do
    [dest] <- allocRegs [r]
    return dest

free :: Var -> AsmEval ()
free var = do
    ds <- gets rDests
    case Map.lookup var ds of
        Just (regs, addrs) -> do
            forM_ regs (cleanReg var)
            forM_ addrs (cleanAddr var)
        -- For string literals
        Nothing -> return ()

blockEndCleanup :: AsmEval ()
blockEndCleanup = do
    st <- get
    forM_ (rLiveOut st) (\(v, a) -> do
        dest <- allocReg (RAny, Just (VI v))
        when (DMem a /= dest) (
            case dest of
                DReg _ -> emitInstr $ IMov SDW (DMem a) dest
                DMem _ -> do
                    dest' <- allocReg (RReg, Just (VI v))
                    emitInstr $ IMov SDW (DMem a) dest'
                _ -> error "Internal error: blockEndCleanup"))
    let diff = rNextLoc st - rOldNextLoc st
    unless (rIsMain st || diff == 0)
           (emitInstr $ IAdd SDW (DReg SP) (DConst $ 4 * diff))

updateLiveIn :: [Var] -> Mapping -> State -> State
updateLiveIn vars mapp st = let
    addrs = map (`unsafeLookup` mapp) vars
  in foldr (uncurry _insAddr) st (zip vars addrs)

-- Is should be called when var was assigned. We should invalidate all the
-- previous locations.
updateDest :: Var -> Dest -> AsmEval ()
updateDest var (DReg reg) = do
    ds_ <- gets rDests
    let ds = case Map.lookup var ds_ of
                Nothing -> Map.insert var ([], []) ds_
                Just _ -> ds_
    modify (\st -> st {rDests = ds})
    let (regs, addrs) = unsafeLookup var ds
    let regs' = List.delete reg regs
    forM_ regs' (cleanReg var)
    forM_ addrs (cleanAddr var)
    insReg var reg
updateDest _ _ = error "Internal error: updateDest"

_insAddr :: Var -> Addr -> State -> State
_insAddr var addr st = let
    ds = rDests st
    (regs, addrs) = unsafeLookup var ds
    st' = st {rMConts = Map.insert addr (Just var) (rMConts st),
              rMFree = List.delete addr (rMFree st),
              rDests = Map.insert var (regs, addr : addrs) ds}
  in if addr `elem` addrs then st else st'

insAddr :: Var -> Addr -> AsmEval ()
insAddr var addr = modify (_insAddr var addr)

insReg :: Var -> Reg -> AsmEval ()
insReg var reg = do
    ds <- gets rDests
    let (regs, addrs) = unsafeLookup var ds
    unless (reg `elem` regs) (
        modify (\st -> st {rRConts = Map.insert reg (Just var) (rRConts st),
                        rRFree = List.delete reg (rRFree st),
                        rDests = Map.insert var (reg : regs, addrs) ds}))

cleanReg :: Var -> Reg -> AsmEval ()
cleanReg var reg = do
    ds <- gets rDests
    let (regs, addrs) = unsafeLookup var ds
    when (reg `elem` regs) (
        modify (\st -> st {rRConts = Map.insert reg Nothing (rRConts st),
                        rRFree = reg : rRFree st,
                        rDests = Map.insert var (List.delete reg regs, addrs) ds}))

cleanAddr :: Var -> Addr -> AsmEval ()
cleanAddr var addr = do
    ds <- gets rDests
    let (regs, addrs) = unsafeLookup var ds
    when (addr `elem` addrs) (
        modify (\st -> st {rMConts = Map.insert addr Nothing (rMConts st),
                        rMFree = addr : rMFree st,
                        rDests = Map.insert var (regs, List.delete addr addrs) ds}))

blockReg :: Reg -> AsmEval ()
blockReg reg = modify (\st -> st {rBlocked = reg : rBlocked st})

evalConst :: Const -> Integer
evalConst (CI int) = int
evalConst (CB True) = 1
evalConst (CB False) = 0
evalConst CNull = 0

getByteReg :: Reg -> Reg
getByteReg EAX = AL
getByteReg EBX = BL
getByteReg ECX = CL
getByteReg EDX = DL
getByteReg _ = error "Internal error: getByteReg"
