module Misc where

import qualified Data.Map.Strict as Map
import Data.Maybe(fromMaybe)

import AbsLatte

predefinedFuns :: [(Ident, Type)]
predefinedFuns = [
    (Ident "printInt", TFun tVoid [tInt]),
    (Ident "printString", TFun tVoid [tString]),
    (Ident "error", TFun tVoid []),
    (Ident "readInt", TFun tInt []),
    (strConcat, TFun tString [tString, tString]),
    (Ident "readString", TFun tString []),
    (strComp, TFun tBool [tString, tString])]

exitFunc :: Ident
exitFunc = Ident "__at__exit__"

mallocFunc :: Ident
mallocFunc = Ident "malloc"

strConcat :: Ident
strConcat = Ident "__concat_strings__"

strComp :: Ident
strComp = Ident "__compare_strings__"

arrLen :: Ident
arrLen = Ident "length"

tInt :: Type
tInt = TBasic BTInt

tString :: Type
tString = TBasic BTStr

tBool :: Type
tBool = TBasic BTBool

tVoid :: Type
tVoid = TBasic BTVoid

unsafeLookup :: (Ord a, Show a, Show b) => a -> Map.Map a b -> b
unsafeLookup k m =
    fromMaybe (error $ "unsafeLookup: key " ++ show k ++ "\nmap: " ++ show m)
              (Map.lookup k m)

tIntMin :: Integer
tIntMin = -2147483647

tIntMax :: Integer
tIntMax = 2147483647

isValidInt :: Integer -> Bool
isValidInt i = i >= tIntMin && i <= tIntMax

iDiv :: Integer -> Integer -> Integer
iDiv i1 i2 = signum i1 * signum i2 * (abs i1 `div` abs i2)

iMod :: Integer -> Integer -> Integer
iMod i1 i2 = i1 - (i1 `iDiv` i2) * i2

constMap :: Ord k => [k] -> a -> Map.Map k a
constMap keys val = Map.fromList (zip keys (replicate (length keys) val))

mapMap :: Ord k => (k -> v) -> [k] -> Map.Map k v
mapMap mfun keys = Map.fromList (zip keys (map mfun keys))
