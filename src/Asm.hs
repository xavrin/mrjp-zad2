module Asm where

import AbsLatte(Ident(..))
import qualified Data.Char as Char

data Module = Module StrLits Text

data StrLits = StrLits [StrLit]

data StrLit = Lit Ident String

data Text = Text [FunEntry]

data FunEntry = FunText Ident [Entry] | FunDecl Ident | FunGlobal Ident

data Entry = ELab Ident | EInstr Instr

data Instr =
    IPush Size Dest |
    IPop Size Dest |
    IMov Size Dest Dest |
    IJmp Dest |
    ICmp Size Dest Dest |
    IJe Dest |
    IAdd Size Dest Dest |
    ISub Size Dest Dest |
    IRet |
    ICall Dest |
    ISetGT Dest |
    ISetLT Dest |
    ISetEQ Dest |
    ISetNE Dest |
    ISetGE Dest |
    ISetLE Dest |
    IMul Size Dest Dest |
    IDiv Size Dest |
    ISar Size Dest Dest |
    IXCHG Size Dest Dest |
    ILea Dest Addr

data Size = SB | SW | SDW | SQW

data Dest = DReg Reg | DMem Addr | DConst Integer | DId Ident deriving Eq

data Reg = EAX | EBX | ECX | EDX | ESI | EDI | FP | SP | AL | BL | CL | DL
           deriving (Ord, Eq)

allRegs :: [Reg]
allRegs = [EAX, EBX, ECX, EDX, ESI, EDI]

data Addr = AddrRI Reg Integer | AddrR Reg | AddrI Integer | AddrId Ident |
            AddrRIM Reg Reg Integer Integer | AddrRM Reg Integer Integer
            deriving (Eq, Ord)

instance Show Module where
    show (Module lits text) = show lits ++ show text

instance Show StrLits where
    show (StrLits lits) = let
        body  = concatMap (\lit -> show lit ++ "\n") lits
      in "section .data\n" ++ body ++ "\n"

instance Show StrLit where
    show (Lit (Ident name) str) = name ++ " db " ++ "'" ++ escape str ++ "', 0"

escape :: String -> String
escape (c : str) =
    if needsEscape c then "'," ++ show (Char.ord c) ++ ",'" ++ escape str
                     else c : escape str
escape [] = []

needsEscape :: Char -> Bool
needsEscape c = not (Char.isPrint c && not (Char.isSpace c)) || c == '\''

instance Show Text where
    show (Text funs) = "section .text\n" ++ concatMap show funs

instance Show FunEntry where
    show (FunText (Ident name) entries) = let
        body = concatMap (\entry -> show entry ++ "\n") entries
      in name ++ ":\n" ++ body ++ "\n"
    show (FunDecl (Ident name)) = "extern " ++ name ++ "\n"
    show (FunGlobal (Ident name)) = "global " ++ name ++ "\n"

instance Show Entry where
    show (ELab (Ident lab)) = lab ++ ":"
    show (EInstr instr) = "    " ++ show instr

instance Show Instr where
    show (IPush size dest) = "push " ++ show size ++ " " ++ show dest
    show (IPop size dest) = "pop " ++ show size ++ " " ++ show dest
    show (IMov size dest src) = "mov " ++ show size ++ " " ++ show dest ++
                                ", " ++ show src
    show (IJmp dest) = "jmp " ++ show dest
    show (ICmp size val1 val2) = "cmp " ++ show size ++ " " ++ show val1 ++
                                           ", " ++ show val2
    show (IJe addr) = "je " ++ show addr
    show (IAdd size dest1 dest2) = showTwoArg "add" size dest1 dest2
    show (ISub size dest1 dest2) = showTwoArg "sub" size dest1 dest2
    show IRet = "ret"
    show (ICall dest) = "call " ++ show dest
    show (ISetGT dest) = "setg " ++ show dest
    show (ISetLT dest) = "setl " ++ show dest
    show (ISetEQ dest) = "sete " ++ show dest
    show (ISetNE dest) = "setne " ++ show dest
    show (ISetGE dest) = "setge " ++ show dest
    show (ISetLE dest) = "setle " ++ show dest
    show (IMul size dest1 dest2) = showTwoArg "imul" size dest1 dest2
    show (IDiv size dest) = "idiv " ++ show size ++ " " ++ show dest
    show (ISar size dest1 dest2) = showTwoArg "sar" size dest1 dest2
    show (IXCHG size dest1 dest2) = showTwoArg "xchg" size dest1 dest2
    show (ILea dest addr) = "lea " ++ show dest ++ ", " ++ show (DMem addr)

showTwoArg :: String -> Size -> Dest -> Dest -> String
showTwoArg instr size dest1 dest2 =
    instr ++ " " ++ show size ++ " " ++ show dest1 ++ ", " ++ show dest2

instance Show Size where
    show SB = "byte"
    show SW = "word"
    show SDW = "dword"
    show SQW = "qword"

instance Show Dest where
    show (DReg reg) = show reg
    show (DMem addr) = "[" ++ show addr ++ "]"
    show (DConst int) = show int
    show (DId (Ident name)) = name

instance Show Reg where
    show EAX = "eax"
    show AL = "al"
    show EBX = "ebx"
    show BL = "bl"
    show ECX = "ecx"
    show CL = "cl"
    show EDX = "edx"
    show DL = "dl"
    show ESI = "esi"
    show EDI = "edi"
    show FP = "ebp"
    show SP = "esp"

instance Show Addr where
    show (AddrRI reg int) = show reg ++ " + (" ++ show int ++ ")"
    show (AddrRIM reg1 reg2 mul offset) =
        show reg1 ++ " + " ++ show reg2 ++ " * " ++ "(" ++  show mul ++ ") + " ++
        "(" ++ show offset ++ ")"
    show (AddrRM reg mul offset) =
        show reg ++ " * " ++ "(" ++  show mul ++ ") + " ++
        "(" ++ show offset ++ ")"
    show (AddrR reg) = show reg
    show (AddrI int) = show int
    show (AddrId (Ident name)) = name
