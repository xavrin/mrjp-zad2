module GenIR where

import qualified AbsLatte as L
import qualified IR as I

import AbsLatte(Ident(..))
import Misc(tInt, tBool, tString, tVoid, predefinedFuns, strConcat,
            unsafeLookup, strComp, arrLen)

import Control.Monad.Identity(Identity, runIdentity)
import Control.Monad.Trans.Reader(ReaderT, runReaderT, ask, local, asks)
import Control.Monad.State(gets, modify, StateT, runStateT)
import Control.Monad(forM, foldM, replicateM)
import Control.Exception.Base(assert)
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import qualified Data.List as List

{- (Type of value, isClassVariable) -}
type EnvVal = (L.Type, Bool)

type Code = [I.Stmt]

type Class = (Map.Map Ident Ident, Map.Map Ident Integer, [(Ident, L.Type)])

data Env = Env {
    rVars :: Map.Map Ident EnvVal,
    rNames :: Map.Map Ident Ident, -- new unique temporary names
    rClasses :: Map.Map Ident Class,
    rInhGraph :: Map.Map Ident (Maybe Ident),
    rCurCls :: Maybe Ident
} deriving Show

data State = State {
    rNextLab :: Integer,
    rNextTmp :: Integer,
    rStrs :: Map.Map String Ident
}

type IREval a = ReaderT Env (StateT State Identity) a

runGenIR :: IREval a -> a
runGenIR m = let
    pv = zip (map snd predefinedFuns) (replicate (length predefinedFuns) False)
    preds = zip (map fst predefinedFuns) pv
    defEnv = Env {rVars = Map.fromList preds,
                  rNames = Map.empty,
                  rClasses = Map.empty,
                  rInhGraph = Map.empty,
                  rCurCls = Nothing}
    defState = State {rNextLab = 0, rNextTmp = 0, rStrs = Map.empty}
  in fst $ runIdentity (runStateT (runReaderT m defEnv) defState)

genIR :: L.Prog -> I.Prog
genIR = runGenIR . genProg

genProg :: L.Prog -> IREval I.Prog
genProg (L.PProg topDefs) = do
    env <- ask
    env' <- foldM getTopDefSig env topDefs
    env'' <- resolveInh (Map.keys $ rClasses env') env'
    funs <- foldM (\l def -> local (const env'') (genTopDef l def)) [] topDefs
    strs <- gets rStrs
    let lits = map (\(str, ident) -> I.DLit ident str) (Map.toList strs)
    return $ I.Prog (lits ++ funs)

genTopDef :: [I.TopDef] -> L.TopDef -> IREval [I.TopDef]
genTopDef funs_ (L.TopClDefB clsIdent members) = let
    genMeth funs (L.MFun (L.FFnDef rTyp fIdent args block)) = do
        clsMap <- asks rClasses
        let (fMap, _, as) = unsafeLookup clsIdent clsMap
        let fIdent' = unsafeLookup fIdent fMap
        let thisArg = L.AArg (L.TClass (L.CTClass clsIdent)) this
        let args' = thisArg : args
        let fnDef = L.TopFnDef (L.FFnDef rTyp fIdent' args' block)
        let ml = zip (map fst as)
                     (zip (map snd as) (replicate (length as) True))
        env_ <- ask
        let vars' = Map.fromList ml `Map.union` rVars env_
        let meths = Map.map (\fi -> (fst (fi `unsafeLookup` vars'), True)) fMap
        let vars'' = meths `Map.union` vars'
        local (\env -> env {rCurCls = Just clsIdent, rVars = vars''})
              (genTopDef funs fnDef)
    genMeth funs' _ = return funs'
  in do
    clsMap <- asks rClasses
    let (_, _, as) = unsafeLookup clsIdent clsMap
    let struct = I.DSt clsIdent (map (convType . snd) as)
    funs' <- foldM genMeth funs_ members
    return (struct : funs')
genTopDef funs (L.TopClDefEx clsIdent _ members) =
    genTopDef funs (L.TopClDefB clsIdent members)
genTopDef funs (L.TopFnDef (L.FFnDef typ ident args block)) = do
    let convArgName (L.AArg _ ident') env = do
        tmp <- nextTmp
        return env {rNames = Map.insert ident' tmp (rNames env)}
    env <- ask
    env' <- foldM (flip convArgName) env args
    let foldFun (L.AArg t id') l = I.Arg (unsafeLookup id' (rNames env'))
                                         (convType t) : l
    let args' = foldr foldFun [] args
    let vars = foldr (\(L.AArg t id') vars' -> Map.insert id' (t, False) vars')
                     (rVars env')
                     args
    stmts <- local (const $ env' {rVars = vars}) (genBlock block)
    -- We want to always have return at the end of code.
    let stmts' = fixRet (convType typ) stmts
    return $ I.DF (convType typ) ident args' stmts' : funs

getTopDefSig :: Env -> L.TopDef -> IREval Env
getTopDefSig env (L.TopFnDef (L.FFnDef typ ident args _)) = do
    let fun = L.TFun typ (foldr (\(L.AArg t _) l -> t : l) [] args)
    return $ updateVars ident (fun, False) env
getTopDefSig env_ (L.TopClDefB clsIdent members) = let
    genMemVar typ (ms, atm, as) name = return (ms, atm, (name, typ) : as)
    genMember cls (L.MVar typ idents) =
        foldM (genMemVar typ) cls idents
    genMember (ms, atm, as) (L.MFun (L.FFnDef _ fIdent _ _)) = do
        let methName = createMethName clsIdent fIdent
        let ms' = Map.insert fIdent methName ms
        return (ms', atm, as)
    genFun env (L.MFun (L.FFnDef rTyp fIdent args block)) = do
        let thisArg = L.AArg (L.TClass (L.CTClass clsIdent)) this
        let args' = thisArg : args
        let methName = createMethName clsIdent fIdent
        getTopDefSig env (L.TopFnDef (L.FFnDef rTyp methName args' block))
    genFun env _ = return env
  in do
    (ms, _, as_) <- foldM genMember (Map.empty, Map.empty, []) members
    let as = reverse as_
    let atm = Map.fromList $ zip (map fst as) [0..]
    let cls' = (ms, atm, as)
    let env = env_ {rClasses = Map.insert clsIdent cls' (rClasses env_)}
    env' <- foldM genFun env members
    return $ env' {rInhGraph = Map.insert clsIdent Nothing (rInhGraph env')}
getTopDefSig env (L.TopClDefEx cls baseCls members) = do
    env' <- getTopDefSig env (L.TopClDefB cls members)
    return $ env' {rInhGraph = Map.insert cls (Just baseCls) (rInhGraph env')}

resolveInh :: [Ident] -> Env -> IREval Env
resolveInh classes env = do
    res <- foldM (\(vis, env') cls ->
                  checkInh cls (Set.insert cls vis, env'))
                 (Set.empty, env) classes
    return (snd res)

checkInh :: Ident -> (Set.Set Ident, Env) -> IREval (Set.Set Ident, Env)
checkInh curCls (visited, env) = let
    mergeClasses (bms, _, bas) (ms, _, as) = let
        as' = bas ++ as
        idents = map fst as'
        atm' = Map.fromList $ zip idents [0..]
      in (bms `Map.union` ms, atm', bas ++ as)
  in case unsafeLookup curCls (rInhGraph env) of
    Nothing -> return (visited, env)
    Just baseCls -> do
        (vis', env') <-
            if Set.member baseCls visited then
                return (visited, env)
            else let
                visited' = Set.insert baseCls visited
                in checkInh baseCls (visited', env)
        let base = unsafeLookup baseCls (rClasses env')
        let cur = unsafeLookup curCls (rClasses env')
        let cls' = mergeClasses base cur
        return (vis',
                env' {rClasses = Map.insert curCls cls' (rClasses env')})

genBlock :: L.Block -> IREval Code
genBlock (L.BBlock stmts) = genStmts stmts

genStmts :: [L.Stmt] -> IREval Code
genStmts [] = return []
genStmts (L.SDecl typ items : stmts) = do
    env <- ask
    (env', code) <- foldM (genItem typ) (env, []) items
    code' <- local (const env') (genStmts stmts)
    return $ code ++ code'
genStmts (stmt : stmts) = do
    code <- genStmt stmt
    code' <- genStmts stmts
    return $ code ++ code'

genStmt :: L.Stmt -> IREval Code
genStmt L.SEmpty = return []
genStmt (L.SBlock block) = genBlock block
genStmt L.SDecl {} = error "Internal error: genStmt"
genStmt (L.SAss expr1 expr2) = do
    b <- isMemAcc expr1
    typ <- getType expr2
    (rval, rhsExprCode) <- genExpr expr2
    if b then do
        (lval, lhsExprCode) <- genPtr expr1
        let storeCode = [I.SStore typ lval rval]
        return $ rhsExprCode ++ lhsExprCode ++ storeCode
    else do
        (I.VI ident, []) <- genExpr expr1
        let assCode = [I.SAss typ ident rval]
        return $ rhsExprCode ++ assCode
genStmt (L.SIncr expr) =
    genStmt (L.SAss expr (L.EAdd expr L.OPlus (L.ELitInt 1)))
genStmt (L.SDecr expr) =
    genStmt (L.SAss expr (L.EAdd expr L.OMinus (L.ELitInt 1)))
genStmt (L.SRet expr) = do
    (val, exprCode) <- genExpr expr
    return $ exprCode ++ [I.SRet val]
genStmt L.SVRet = return [I.SVRet]
genStmt (L.SCond expr stmt) = genStmt (L.SCondElse expr stmt L.SEmpty)
genStmt (L.SCondElse expr stmt1 stmt2) = do
    (val, exprCode) <- genExpr expr
    trueCode <- genStmt stmt1
    falseCode <- genStmt stmt2
    [lab, lab', lab''] <- replicateM 3 nextLab
    return $ exprCode ++ [I.SCJmp val lab lab'] ++ [I.SLab lab] ++ trueCode ++
             [I.SJmp lab''] ++ [I.SLab lab'] ++ falseCode ++ [I.SJmp lab''] ++
             [I.SLab lab'']
genStmt (L.SWhile expr stmt) = do
    (val, exprCode) <- genExpr expr
    bodyCode <- genStmt stmt
    [lab, lab', lab''] <- replicateM 3 nextLab
    return $ [I.SJmp lab'] ++ [I.SLab lab] ++ bodyCode ++ [I.SJmp lab'] ++
             [I.SLab lab'] ++ exprCode ++ [I.SCJmp val lab lab''] ++
             [I.SLab lab'']
genStmt (L.SForeach ltyp ident1 ident2 stmt) = do
    li <- nextTmp
    let ini = [L.SDecl (L.TBasic L.BTInt) [L.SInit li (L.ELitInt 0)],
               L.SDecl ltyp [L.SNoInit ident1]]
    let arrAss = L.SAss (L.EVar ident1) (L.EArray (L.EVar ident2) (L.EVar li))
    let cond = L.EOrd (L.EVar li) L.OLTH
                      (L.EMemVar (L.EVar ident2) arrLen)
    let while = L.SWhile cond (L.SBlock $ L.BBlock [arrAss, stmt,
                                                    L.SIncr (L.EVar li)])
    genStmt (L.SBlock $ L.BBlock (ini ++ [while]))
genStmt (L.SExp expr) = do
    (_, code) <- genExpr expr
    return code

genExpr :: L.Expr -> IREval (I.Val, Code)
genExpr (L.EVar ident) = do
    env <- ask
    let (_, b) = unsafeLookup ident (rVars env)
    if b then
        genExpr (L.EMemVar (L.EVar this) ident)
    else
        return (I.VI $ unsafeLookup ident (rNames env), [])
genExpr (L.ELitInt int) = return (I.VC $ I.CI int, [])
genExpr L.ELitTrue = return (I.VC $ I.CB True, [])
genExpr L.ELitFalse = return (I.VC $ I.CB False, [])
genExpr L.ENull = return (I.VC I.CNull, [])
genExpr expr@(L.EApp ident exprs) = do
    env <- ask
    let (_, b) = unsafeLookup ident (rVars env)
    if b then
        genExpr (L.EMemFun (L.EVar this) ident exprs)
    else do
        ret <- forM exprs genExpr
        let (args, argsCode) = (map fst ret, concatMap snd ret)
        tmp <- nextTmp
        typ <- getType expr
        return (I.VI tmp, argsCode ++ [I.SCall typ tmp ident args])
genExpr (L.EString str) = do
    strs <- gets rStrs
    case Map.lookup str strs of
        Nothing -> do
            tmp <- nextTmp
            modify (\st -> st {rStrs = Map.insert str tmp (rStrs st)})
            return (I.VI tmp, [])
        Just ident -> return (I.VI ident, [])
genExpr expr@(L.EArray expr1 _) = do
    (ptr, ptrCode) <- genPtr expr
    I.TArr typ <- getType expr1
    var <- nextTmp
    return (I.VI var, ptrCode ++ [I.SLoad typ var ptr])
genExpr L.EACast {} = return (I.VC I.CNull, [])
genExpr L.EExprCast {} = return (I.VC I.CNull, [])
genExpr (L.EClInit typ) = do
    tmp <- nextTmp
    return (I.VI tmp, [I.SAlloc (convType typ) tmp (I.VC (I.CI 1))])
genExpr (L.EArrInit typ expr) = do
    let convVal val = case val of
                          I.VI var -> L.EVar var
                          I.VC (I.CI int) -> L.ELitInt int
                          _ -> error "Internal error: genExpr arrInit"
    (len, lenCode) <- genExpr expr
    arr <- nextTmp
    i <- nextTmp
    let updateVar var typ' env = env {rNames = Map.insert var var (rNames env),
                                     rVars = Map.insert var typ' (rVars env)}
    {- We have to initialize the array. -}
    let lenInitStmt = L.SAss (L.EMemVar (L.EVar arr) arrLen) (convVal len)
    let elemInitStmt = L.SAss (L.EArray (L.EVar arr) (L.EVar i))
                              (getDefValue typ)
    let cond = L.EOrd (L.EVar i) L.OLTH (L.EMemVar (L.EVar arr) arrLen)
    let whileStmts = [L.SAss (L.EVar i) (L.ELitInt 0),
                      L.SWhile cond (L.SBlock $ L.BBlock [elemInitStmt,
                                                          L.SIncr (L.EVar i)])]
    let arrType = L.TArray (L.ATArray typ)
    let modifyEnv = updateVar arr (arrType, False) . updateVar i (tInt, False)
    let modifyEnv' = case len of
                         I.VI var -> modifyEnv . updateVar var (tInt, False)
                         _ -> modifyEnv
    initCode <- local modifyEnv' (genStmts (lenInitStmt : whileStmts))
    return (I.VI arr, lenCode ++ [I.SAlloc (I.TArr $ convType typ) arr len] ++
                      initCode)
genExpr memExpr@L.EMemVar {} = do
    memTyp <- getType memExpr
    (ptr, ptrCode) <- genPtr memExpr
    memVar <- nextTmp
    return (I.VI memVar, ptrCode ++ [I.SLoad memTyp memVar ptr])
genExpr (L.EMemFun expr memIdent exprs) = do
    L.TClass (L.CTClass clsIdent) <- getTypeL expr
    classes <- asks rClasses
    let (meths, _, _) = unsafeLookup clsIdent classes
    let fIdent = unsafeLookup memIdent meths
    genExpr (L.EApp fIdent (expr : exprs))
genExpr (L.ENeg expr) = do
    (val, code) <- genExpr expr
    tmp <- nextTmp
    return (I.VI tmp, code ++ [I.S4 I.TInt tmp (I.VC $ I.CI 0) I.AMinus val])
genExpr (L.ENot expr) = do
    (val, code) <- genExpr expr
    tmp <- nextTmp
    return (I.VI tmp, code ++ [I.SNot tmp val])
genExpr (L.EMul expr1 op expr2) = genS4 I.TInt expr1 (convMulOp op) expr2
genExpr (L.EAdd expr1 op expr2) = do
    typ <- getTypeL expr1
    if typ == tString then do
        (str', code') <- genExpr expr2
        (str, code) <- genExpr expr1
        tmp <- nextTmp
        return (I.VI tmp, code ++ code' ++
                          [I.SCall I.strType tmp strConcat [str, str']])
    else genS4 I.TInt expr1 (convAddOp op) expr2
genExpr (L.EOrd expr1 op expr2) = genS4 I.TBool expr1 (convOrdOp op) expr2
genExpr (L.EEq expr1 op expr2) = do
    typ <- getTypeL expr1
    if typ == tString then do
        (str, code) <- genExpr expr1
        (str', code') <- genExpr expr2
        tmp <- nextTmp
        let code'' = code ++ code' ++ [I.SCall I.TBool tmp strComp [str, str']]
        if op == L.OEQU then
            return (I.VI tmp, code'')
        else do
            tmp' <- nextTmp
            return (I.VI tmp', code'' ++ [I.SNot tmp' (I.VI tmp)])
    else genS4 I.TBool expr1 (convEqOp op) expr2
genExpr (L.EAnd expr1 expr2) = genBoolJmp False expr1 expr2
genExpr (L.EOr expr1 expr2) = genBoolJmp True expr1 expr2
genExpr L.ESelf = genExpr (L.EVar this)

genBoolJmp :: Bool -> L.Expr -> L.Expr -> IREval (I.Val, Code)
genBoolJmp isOr expr1 expr2 = do
    (val, code) <- genExpr expr1
    (val', code') <- genExpr expr2
    [ltrue, lfalse, lend] <- replicateM 3 nextLab
    tmp <- nextTmp
    let (l1, l2) = if isOr then (lfalse, ltrue) else (ltrue, lfalse)
    return (I.VI tmp, code ++ [I.SCJmp val ltrue lfalse] ++ [I.SLab l1] ++
            code' ++ [I.SAss I.TBool tmp val'] ++ [I.SJmp lend] ++
            [I.SLab l2] ++ [I.SAss I.TBool tmp (I.VC $ I.CB isOr)] ++
            [I.SJmp lend] ++ [I.SLab lend])

genS4 :: I.Type -> L.Expr -> I.Op -> L.Expr -> IREval (I.Val, Code)
genS4 typ expr1 op expr2 = do
    (val, code) <- genExpr expr1
    (val', code') <- genExpr expr2
    tmp <- nextTmp
    return (I.VI tmp, code ++ code' ++ [I.S4 typ tmp val op val'])

updateVars :: Ident -> EnvVal -> Env -> Env
updateVars ident value env = env {rVars = Map.insert ident value (rVars env)}

nextLab :: IREval I.Lab
nextLab = do
    i <- gets rNextLab
    modify (\st -> st {rNextLab = rNextLab st + 1})
    return $ Ident $ "L" ++ show i

nextTmp :: IREval I.Var
nextTmp = do
    i <- gets rNextTmp
    modify (\st -> st {rNextTmp = rNextTmp st + 1})
    return $ Ident $ "t" ++ show i

isMemAcc :: L.Expr -> IREval Bool
isMemAcc (L.EVar var) = do
    mVar <- asks rVars
    return (snd (unsafeLookup var mVar))
isMemAcc _ = return True

genPtr :: L.Expr -> IREval (I.Val, Code)
genPtr memExpr@(L.EMemVar expr name) = do
    ltyp <- getTypeL expr
    let typ = convType ltyp
    case ltyp of
        L.TArray _ -> do
            (arr, arrCode) <- genExpr (assert (name == arrLen) expr)
            ptr <- nextTmp
            return (I.VI ptr, arrCode ++
                              [I.SElPtr (I.TPtr I.TInt) ptr typ arr
                                        (I.VC $ I.CI (-1))])
        L.TClass (L.CTClass clsIdent) -> do
            memTyp <- getType memExpr
            classes <- asks rClasses
            let (_, atm, _) = unsafeLookup clsIdent classes
            (obj, objCode) <- genExpr expr
            ptr <- nextTmp
            let idx = unsafeLookup name atm
            return (I.VI ptr, objCode ++
                              [I.SElPtr (I.TPtr memTyp) ptr typ obj
                                        (I.VC $ I.CI idx)])

        _ -> error "Internal error: genPtr"
genPtr (L.EArray expr1 expr2) = do
    atyp@(I.TArr typ) <- getType expr1
    (arr, arrCode) <- genExpr expr1
    (idx, idxCode) <- genExpr expr2
    tmp <- nextTmp
    return (I.VI tmp, arrCode ++ idxCode ++
                      [I.SElPtr (I.TPtr typ) tmp atyp arr idx])
genPtr (L.EVar var) = genPtr (L.EMemVar L.ESelf var)
genPtr e = error $ "Internal error: genPtr" ++ show e

genItem :: L.Type -> (Env, Code) -> L.Item -> IREval (Env, Code)
genItem typ acc (L.SNoInit ident) =
    genItem typ acc (L.SInit ident (getDefValue typ))
genItem typ (env, code) (L.SInit ident expr) = do
    (tmp, code') <- local (const env) (genExpr expr)
    tmp' <- nextTmp
    let env' = updateVars ident (typ, False) env
    let env'' = env' {rNames = Map.insert ident tmp' (rNames env')}
    return (env'', code ++ code' ++ [I.SAss (convType typ) tmp' tmp])

getDefValue :: L.Type -> L.Expr
getDefValue (L.TBasic L.BTInt) = L.ELitInt 0
getDefValue (L.TBasic L.BTBool) = L.ELitFalse
getDefValue (L.TArray atyp) = L.EACast atyp L.ENull
getDefValue (L.TClass (L.CTClass ident)) = L.EExprCast (L.EVar ident) L.ENull
getDefValue (L.TBasic L.BTStr) = L.EString ""
getDefValue _ = error "Internal error: getDefValue"

convType :: L.Type -> I.Type
convType (L.TBasic L.BTInt) = I.TInt
convType (L.TBasic L.BTBool) = I.TBool
convType (L.TBasic L.BTStr) = I.strType
convType (L.TBasic L.BTVoid) = I.TVoid
convType (L.TArray (L.ATArray typ)) = I.TArr (convType typ)
convType (L.TClass (L.CTClass ident)) = I.TSt ident
convType (L.TFun ret args) =  I.TFun (convType ret) (map convType args)

convAddOp :: L.AddOp -> I.Op
convAddOp L.OPlus = I.APlus
convAddOp L.OMinus = I.AMinus

convMulOp :: L.MulOp -> I.Op
convMulOp L.OTimes = I.ATimes
convMulOp L.ODiv = I.ADiv
convMulOp L.OMod = I.AMod

convOrdOp :: L.OrdOp -> I.Op
convOrdOp L.OLTH = I.RLT
convOrdOp L.OLE = I.RLE
convOrdOp L.OGTH = I.RGT
convOrdOp L.OGE = I.RGE

convEqOp :: L.EqOp -> I.Op
convEqOp L.OEQU = I.REQ
convEqOp L.ONE = I.RNE

getType :: L.Expr -> IREval I.Type
getType expr = do
    ltyp <- getTypeL expr
    return $ convType ltyp

getTypeL :: L.Expr -> IREval L.Type
getTypeL (L.EVar ident) = do
    mvars <- asks rVars
    let Just (typ, _) = Map.lookup ident mvars
    return typ
getTypeL L.ELitInt {} = return tInt
getTypeL L.ELitTrue {} = return tBool
getTypeL L.ELitFalse {} = return tBool
getTypeL L.ENull = return tVoid
getTypeL (L.EApp ident _) = do
    mvars <- asks rVars
    let Just (L.TFun typ _, _) = Map.lookup ident mvars
    return typ
getTypeL L.EString {} = return tString
getTypeL (L.EArray expr _) = do
    L.TArray (L.ATArray typ) <- getTypeL expr
    return typ
getTypeL (L.EACast atyp _) = return (L.TArray atyp)
getTypeL (L.EExprCast expr _) = do
    let L.EVar clsIdent = expr
    return (L.TClass (L.CTClass clsIdent))
getTypeL (L.EClInit typ) = return typ
getTypeL (L.EArrInit typ _) = return (L.TArray $ L.ATArray typ)
getTypeL (L.EMemVar expr mem) = do
    typ <- getType expr
    case typ of
        I.TArr _ ->
            if mem == arrLen then return tInt else error "Internal error"
        I.TSt clsIdent -> do
            classes <- asks rClasses
            let (_, atm, as) = unsafeLookup clsIdent classes
            let idx = unsafeLookup mem atm
            return $ snd (as `List.genericIndex` idx)
        _ -> error "Internal error: getTypeL"
getTypeL (L.EMemFun expr memIdent _) = do
    I.TSt clsIdent <- getType expr
    getTypeL (L.EApp (createMethName clsIdent memIdent) [])

getTypeL L.ENeg {} = return tInt
getTypeL L.ENot {} = return tBool
getTypeL L.EMul {} = return tInt
getTypeL (L.EAdd expr _ _) = getTypeL expr -- it can be a string or int
getTypeL L.EOrd {} = return tBool
getTypeL L.EEq {} = return tBool
getTypeL L.EAnd {} = return tBool
getTypeL L.EOr {} = return tBool
getTypeL L.ESelf = do
    Just clsIdent <- asks rCurCls
    return (L.TClass (L.CTClass clsIdent))

{- Inserting the return instruction at the very end of code in case it's not
   there. -}
fixRet :: I.Type -> [I.Stmt] -> [I.Stmt]
fixRet _ [I.SVRet] = [I.SVRet]
fixRet _ [ret@I.SRet {}] = [ret]
fixRet typ [stmt] = [stmt, getDefInstr typ]
fixRet typ [] = [getDefInstr typ]
fixRet typ (stmt : stmts) = stmt : fixRet typ stmts

getDefInstr :: I.Type -> I.Stmt
getDefInstr I.TVoid = I.SVRet
getDefInstr typ = I.SRet (getDefIRetValue typ)

getDefIRetValue :: I.Type -> I.Val
getDefIRetValue I.TInt = I.VC (I.CI 0)
getDefIRetValue I.TBool = I.VC (I.CB False)
getDefIRetValue I.TArr {} = I.VC I.CNull
getDefIRetValue I.TPtr {} = I.VC I.CNull
getDefIRetValue I.TSt {} = I.VC I.CNull
getDefIRetValue _ = error "Internal error: getDefIRetValue"

this :: Ident
this = Ident "this"

createMethName :: Ident -> Ident -> Ident
createMethName (Ident clsName) (Ident methName) =
    Ident (clsName ++ "." ++ methName)
