module IR where

import AbsLatte(Ident(..))
import Data.List(intercalate)

data Prog = Prog [TopDef] deriving (Eq,Ord,Read)

data TopDef = DF Type Ident [Arg] [Stmt] | DLit Ident String | DSt Ident [Type]
              deriving (Eq,Ord,Read)

data Arg = Arg Ident Type deriving (Eq,Ord,Read)

data Type = TInt | TBool | TVoid | TFun Type [Type] | TArr Type | TPtr Type |
            TSt Ident deriving (Eq,Ord,Read)

type Var = Ident

data Val = VI Ident | VC Const deriving (Eq,Ord,Read)

data Const = CI Integer | CB Bool | CNull deriving (Eq,Ord,Read)

type Lab = Ident

{- A few words about the type system for this IR:
   Array and structures are different from pointers, however they are
   references to the actual content.
   Store and Load operate on pointers. So if someone wants to use structs'
   or arrays' memory, he or she must use SElPtr first to retrieve the
   pointer first. -}

{- We demand from the code generators that every block ends with either a jump
   or a return. Otherwise the behaviour is implementation defined. -}
data Stmt =
    SJmp Lab |
    SCJmp Val Lab Lab|
    S4 Type Var Val Op Val |
    SAss Type Var Val |
    SNot Var Val |
    SLoad Type Var Val | -- Type - type of value loaded
    SStore Type Val Val | -- Type - type of value being stored
                          -- 1st val - lvalue, 2nd val - rvalue
    SElPtr Type Var Type Val Val | -- type of var, var being assigned,
                                   -- type of complex type, complex type,
                                   -- index into the comlex type
                                   -- In case of array -1 is length, 0 is fst
                                   -- elem, 1 the snd, etc.
    SLab Lab |
    SRet Val |
    SVRet |
    SAlloc Type Var Val | -- type of allocated object, TArr for arrays
                          -- 1st - result,
                          -- 2nd - size of array (should be 1 for objects)
    SCall Type Var Ident [Val] |
    SPhi Type Var [(Lab, Val)]
    deriving (Eq,Ord,Read)

data Op = RGT | RLT | REQ | RNE | RGE | RLE | APlus | AMinus | ATimes | ADiv |
          AMod deriving (Eq,Ord,Read)

isRelOp :: Op -> Bool
isRelOp op = isEqOp op || isOrdOp op

isOrdOp :: Op -> Bool
isOrdOp RGT = True
isOrdOp RLT = True
isOrdOp RLE = True
isOrdOp RGE = True
isOrdOp _ = False

isEqOp :: Op -> Bool
isEqOp REQ = True
isEqOp RNE = True
isEqOp _ = False

strType :: Type
strType = TSt (Ident "string")

instance Show Prog where
    show (Prog topdefs) = concatMap (\def -> show def ++ "\n") topdefs

instance Show TopDef where
    show (DF typ (Ident name) args stmts) =
        show typ ++ " " ++ name ++ "(" ++ showL show args ++ ") {\n" ++
        concatMap (\stmt -> showS stmt ++ "\n") stmts ++ "}\n"
    show (DLit (Ident name) str) = "const " ++ name ++ " = " ++ str ++ "\n"
    show (DSt (Ident name) types) =
        "struct " ++ name ++ "\n {" ++ showL show types ++ "}"

instance Show Stmt where
    show (SJmp lab) = "goto " ++ show lab
    show (SCJmp val lab1 lab2) = "if " ++ show val ++ " then " ++ show lab1
                                                   ++ " else " ++ show lab2
    show (S4 typ var val1 op val2) = show typ ++ " " ++ show var ++ " = " ++
                                     show val1 ++ " " ++ show op ++ " " ++
                                     show val2
    show (SAss typ var val) = show typ ++ " " ++ show var ++ " = " ++ show val
    show (SNot var val) = show var ++ " = " ++ "!" ++ show val
    show (SLoad typ var val) = show typ ++ " " ++ show var ++ " = *" ++ show val
    show (SStore _ val1 val2) = "*" ++ show val1 ++ " = " ++ show val2
    show (SElPtr typ var ctyp val1 val2) =
        show typ ++ " " ++ show var ++ " = " ++ "getPtr " ++ show ctyp ++ " " ++
        show val1 ++ " " ++ show val2
    show (SLab lab) = show lab ++ ":"
    show (SRet val) = "return " ++ show val
    show SVRet = "vreturn"
    show (SAlloc typ var val) = show typ ++ " " ++ show var ++ " = new "
                             ++ show typ ++ " [" ++ show val ++ "]"
    show (SCall typ var ident vals) =
        show typ ++ " " ++ show var ++ " = " ++ show ident ++ "(" ++
        showL show vals ++ ")"
    show (SPhi typ var opts) = show typ ++ " " ++ show var ++ " = phi(" ++
                               showL showOpt opts ++ ")"

instance Show Type where
    show TInt = "int"
    show TBool = "bool"
    show TVoid = "void"
    show (TFun typ types) = show typ ++ "(" ++ showL show types ++ ")"
    show (TArr typ) = show typ ++ "[]"
    show (TPtr typ) = show typ ++ "*"
    show (TSt ident) = "struct " ++ show ident

instance Show Val where
    show (VI ident) = show ident
    show (VC c) = show c

instance Show Const where
    show (CI int) = show int
    show (CB b) = show b
    show CNull = "null"

instance Show Arg where
    show (Arg ident typ) = show typ ++ " " ++ show ident

instance Show Op where
    show RGT = ">"
    show RLT = "<"
    show REQ = "=="
    show RNE = "!="
    show RLE = "<="
    show RGE = ">="
    show APlus = "+"
    show AMinus = "-"
    show ATimes = "*"
    show ADiv = "/"
    show AMod = "%"

showOpt :: (Lab, Val) -> String
showOpt (lab, val) = show lab ++ ": " ++ show val

showL :: (a -> String) -> [a] -> String
showL f as = intercalate "," (map f as)

showS :: Stmt -> String
showS stmt@SLab {} = show stmt
showS stmt = "    " ++ show stmt

isConst :: Val -> Bool
isConst VC {} = True
isConst _ = False

isArr :: Type -> Bool
isArr TArr {} = True
isArr _ = False
