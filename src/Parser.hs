module Parser where

import ParLatte(myLexer, pProg)
import AbsLatte(Prog)
import ErrM(Err)

parse :: String -> Err Prog
parse = pProg . myLexer
